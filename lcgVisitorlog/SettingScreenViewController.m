//
//  SettingScreenViewController.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/18/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "SettingScreenViewController.h"
#import "MainViewController.h"
@interface SettingScreenViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgViewbackground;
@property (strong, nonatomic) IBOutlet UIView *viewLower;
@property (strong, nonatomic) IBOutlet UILabel *lblSchoolid;
@property (strong, nonatomic) IBOutlet UILabel *lblUploadLogo;
@property (strong, nonatomic) IBOutlet UITextField *txtSchoolID;
@property (strong, nonatomic) IBOutlet UITextField *txtUploadLogo;

@end

@implementation SettingScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    ;
    [super viewDidLoad];
    self.viewLower.backgroundColor=[UIColor colorWithRed:26/255.0f green:127/255.0f blue:194/255.0f alpha:1.0f];
    [self drawLine:self.txtSchoolID];
    [self drawLine:self.txtUploadLogo];
    [self.txtUploadLogo setRightViewMode:UITextFieldViewModeAlways];
    self.txtUploadLogo.rightView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo.png"]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self navigationManagement];
    
    MainViewController *objMainViewController =[[MainViewController  alloc]init];
    
    
    [ objMainViewController changeFont:self.txtSchoolID];
    [ objMainViewController changeFont:self.txtUploadLogo];
    [objMainViewController changeCursorcolor:self.txtUploadLogo];
    [ objMainViewController changeCursorcolor:self.txtSchoolID];
    [ self changelabel:self.lblSchoolid];
    [ self changelabel:self.lblUploadLogo];

    
}


-(void)changelabel:(UILabel *)label
{
    label.font=[UIFont fontWithName:@"Roboto-Light" size:18];
}

-(void)titleForNavigation
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"SETTINGS", @"");
    [label sizeToFit];
    self.navigationItem.titleView=label;
    
    
}

-(void)backBtnForNavigation
{
    UIImage *backBtnImage = [UIImage imageNamed:@"back.png"];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(actionBtnBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem  = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setLeftBarButtonItem:backItem];
    
}

-(void)doneButtonForNavigation
{
    UIImage *doneBtnImage = [UIImage imageNamed:@"large-circle.png"];
    
    UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [doneBtn setImage:doneBtnImage forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(actionButtonDone) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneItem  = [[UIBarButtonItem alloc] initWithCustomView:doneBtn];
    self.navigationItem.rightBarButtonItem = doneItem;
}

-(void)navigationManagement
{
    [self titleForNavigation];
    
    // change the background color to black
    //self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self backBtnForNavigation];
    [self doneButtonForNavigation];
}
-(void)actionBtnBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)actionButtonDone
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)drawLine:(UITextField *)textField
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,29, self.view.frame.size.width, 1.8)];
    lineView.backgroundColor = [UIColor whiteColor];
    [textField addSubview:lineView];
    
    
}
@end

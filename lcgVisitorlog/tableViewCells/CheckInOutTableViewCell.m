//
//  CheckInOutTableViewCell.m
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "CheckInOutTableViewCell.h"

@implementation CheckInOutTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        btnImageCheckInOut = [[UIButton alloc] initWithFrame:CGRectMake(60, 10, 45, 45)];
        [btnImageCheckInOut setBackgroundColor:[UIColor clearColor]];
        [btnImageCheckInOut addTarget:self action:@selector(imageBtnOfCellForCheck) forControlEvents:UIControlEventTouchUpInside];
        
        lblFullName = [[UILabel alloc]initWithFrame:CGRectMake(140, 20, 150, 20)];
        [lblFullName setBackgroundColor:[UIColor clearColor]];
        lblFullName.lineBreakMode = NSLineBreakByWordWrapping;
        lblFullName.numberOfLines = 2;
        [lblFullName setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        lblEmail = [[UILabel alloc]initWithFrame:CGRectMake(300, 20, 190, 20)];
        [lblEmail setBackgroundColor:[UIColor clearColor]];
        [lblEmail setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        lblPhone = [[UILabel alloc]initWithFrame:CGRectMake(780, 20, 150, 20)];
        lblPhone.backgroundColor = [UIColor clearColor];
        [lblPhone setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        imageMgmtValidation = [[UIImageView alloc]initWithFrame:CGRectMake(680, 18, 25, 25)];
        [imageMgmtValidation setBackgroundColor:[UIColor clearColor]];
        btnImageMgmtVal = [[UIButton alloc]initWithFrame:CGRectMake(680, 10, 45, 45)];
        [btnImageMgmtVal setBackgroundColor:[UIColor clearColor]];
        btnImageMgmtVal.autoresizingMask=UIViewAutoresizingNone;
        [btnImageMgmtVal addTarget:self action:@selector(imageBtnOfCellForMgmtVal) forControlEvents:UIControlEventTouchUpInside];
        [btnImageMgmtVal setBackgroundColor:[UIColor clearColor]];
        
        lblLeadSource = [[UILabel alloc]initWithFrame:CGRectMake(960, 20, 300, 20)];
        [lblLeadSource setBackgroundColor:[UIColor clearColor]];
        [lblLeadSource setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        lblLeadChilds = [[UILabel alloc]initWithFrame:CGRectMake(1180, 20, 20, 20)];
        [lblLeadChilds setBackgroundColor:[UIColor clearColor]];
        [lblLeadChilds setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        lblReasonToVisit = [[UILabel alloc]initWithFrame:CGRectMake(1250, 20, 300, 20)];
        [lblReasonToVisit setBackgroundColor:[UIColor clearColor]];
        [lblReasonToVisit setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
        
        [self.contentView addSubview:btnImageCheckInOut];
        [self.contentView addSubview:btnImageMgmtVal];
        [self.contentView addSubview:lblFullName];
        [self.contentView addSubview:lblEmail];
        
        [self.contentView addSubview:lblPhone];
        [self.contentView addSubview:lblLeadSource];
        [self.contentView addSubview:lblLeadChilds];
        [self.contentView addSubview:lblReasonToVisit];
        
    }
    return self;
}

-(void)imageBtnOfCellForCheck
{
    self.blockGettingRef();
    
    
}

-(void)imageBtnOfCellForMgmtVal
{
    self.blockForMgmtVal();
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - custom methods

-(NSString *)setValues:(Person *)objPerson
{
    [lblInTime removeFromSuperview];
    [lblOutTime removeFromSuperview];
    
    if([[objPerson checkOutTime] isEqual:@" "]){
        [self setInOutTimeLblConditionallyForOne];
    }else{
        [self setInOutTimeLblConditionallyForBoth];
    }
    [lblInTime setBackgroundColor:[UIColor clearColor]];
    [lblOutTime setBackgroundColor:[UIColor clearColor]];
    [lblInTime setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [lblOutTime setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [self.contentView addSubview:lblInTime];
    [self.contentView addSubview:lblOutTime];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mma MM-dd-yy"];
    
    [btnImageCheckInOut setImage:[UIImage imageNamed:[objPerson imageCheckInOut]] forState:UIControlStateNormal];
    lblFullName.text = [NSString stringWithFormat:@"%@ %@",[objPerson firstName],[objPerson lastName]];
    [btnImageMgmtVal setImage:[UIImage imageNamed:[objPerson imageMgmtValidation]] forState:UIControlStateNormal];
    lblEmail.text = [objPerson email];
    lblPhone.text = [objPerson phone];
    lblInTime.text = [dateFormatter stringFromDate:[objPerson checkInTime]];
    lblOutTime.text = [dateFormatter stringFromDate:[objPerson checkOutTime]];
    lblLeadSource.text = [objPerson leadSource];
    lblLeadChilds.text = [objPerson leadTotalChilds];
    lblReasonToVisit.text = [objPerson reasonToVisit];
    
    
    return [objPerson personType];
}

-(void)setInOutTimeLblConditionallyForBoth{
    lblInTime = [[UILabel alloc]initWithFrame:CGRectMake(497, 20,160, 20)];
    lblOutTime = [[UILabel alloc]initWithFrame:CGRectMake(497, 39, 160, 20)];
    lblInTime.backgroundColor=[UIColor greenColor];
}
-(void)setInOutTimeLblConditionallyForOne{
    
    lblInTime = [[UILabel alloc]initWithFrame:CGRectMake(520, 20, 140, 20)];
    
}


@end

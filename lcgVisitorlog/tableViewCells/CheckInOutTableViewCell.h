//
//  CheckInOutTableViewCell.h
//
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import "CheckInOutViewController.h"

@protocol RefPassProtocol <NSObject>

@end

@interface CheckInOutTableViewCell : UITableViewCell{
    
    Person *person_;
    
    //UIImageView      *imageCheckInOut;
    UIImageView      *imageMgmtValidation;
    UILabel          *lblFullName;
    UILabel          *lblInTime;
    UILabel          *lblOutTime;
    UILabel          *lblPhone;
    UILabel          *lblEmail;
    
    UIButton *btnImageCheckInOut;
    UIButton *btnImageMgmtVal;
    
    UILabel *lblLeadSource;
    UILabel *lblLeadChilds;
    UILabel *lblReasonToVisit;
    
    NSDateFormatter *dateFormatter;
    
}

@property (nonatomic, copy) id <RefPassProtocol> delegate;

-(NSString *)setValues:(Person *)objPerson;

@property (nonatomic,strong) void (^blockGettingRef)();

@property (nonatomic,strong) void (^blockForMgmtVal)();

@end

//
//  DataCheckInOut.m
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "DataCheckInOut.h"

@implementation DataCheckInOut

-(id)init{
    // set the number of rows
    
    [self loadImageCheckInOut];
    [self loadImageMgmtValidation];
    [self loadPersonType];
    [self loadFirstNames];
    [self loadlastNames];
    [self loadEmails];
    [self loadPhoneNumbers];
    [self loadCheckInTime];
    [self loadCheckOutTime];
    
    [self loadLeadChilds];
    [self loadLeadResonToVisit];
    [self loadLeadSource];
    
    return self;
}

#pragma mark - Data loading methods

-(void)loadImageCheckInOut
{
    

    
    self.imageCheckInOut = [[NSMutableArray alloc] initWithCapacity:10];
    for(int i=0; i <10 ;i++){
        if(i%2 == 0){
            [self.imageCheckInOut addObject:@"green-tick.png"];
        }else{
            [self.imageCheckInOut addObject:@"red-tick.png"];        }
    }
    
}

-(void)loadImageMgmtValidation
{
    self.imageMgmtValidation = [[NSMutableArray alloc] init];
    for(int i=0 ;i <10;i++){
        if(i%2 != 0){
            [self.imageMgmtValidation addObject:@"blue-tick.png"];
        }else{
            [self.imageMgmtValidation addObject:@"red-tick.png"];
        }
    }    
}


-(void)loadPersonType
{
    self.personType =[[NSArray alloc]initWithObjects:@"lead",
                 @"lead",
                 @"volunteer",
                 @"partner",
                 @"lead",
                 @"partner",
                 @"partner",
                 @"volunteer",
                 @"lead",
                 @"others",
                 nil];
}
-(void)loadFirstNames
{
    self.firstNamesArr = [[NSArray alloc]initWithObjects:@"Mary",
                @"Ray",
                @"Eve",
                @"Daisy",
                @"Bernie",
                @"Hillary",
                @"Carol",
                @"Edward",
                @"Jaiff",
                @"Tony",
                nil];
}

-(void)loadlastNames
{
    self.lastNamesArr = [[NSArray alloc]initWithObjects:@"Ardon",
                     @"Anderson",
                     @"Anthony",
                     @"Ward",
                     @"Brilliston",
                     @"Sawran",
                     @"Burnet",
                     @"Cullen",
                     @"Marfein",
                     @"Stark",
                     nil];
}

-(void)loadPhoneNumbers
{
    self.phoneArr = [[NSArray alloc]initWithObjects:@"999-095-8358",
                    @"985-474-1632",
                    @"936-584-1574",
                    @"985-468-7881",
                    @"923-150-5487",
                    @"955-746-8741",
                    @"955-746-8741",
                    @"955-787-8754",
                    @"955-746-0000",
                    @"976-546-8788",
                    nil];
}

-(void)loadEmails
{
    self.emailArr =[[NSArray alloc]initWithObjects:@"Ardon@gmail.com",
                   @"Andeson@osscube.com",
                   @"Anthony@lcg.com",
                   @"Ward@yahoo.com",
                   @"Brilliston@live.com",
                   @"Sawran@hotmail.com",
                   @"Burnet@gmail.com",
                   @"Cullen@radiff.com",
                   @"Marfein@aol.co.in",
                   @"Stark@yahoo.co.in",
                   nil];

}

-(void)loadCheckInTime
{
    self.checkInTime =[[NSArray alloc]initWithObjects:@"04:35PM 7-14-2014",
                       @"04:15AM 7-18-2014",
                       @"04:28PM 17-17-2014",
                       @"04:38AM 27-16-2014",
                       @"04:45PM 7-14-2014",
                       @"04:56AM 06-14-2014",
                       @"04:33PM 7-14-2014",
                       @"04:47PM 7-21-2014",
                       @"04:01AM 7-20-2014",
                       @"04:05AM 7-14-2014",
                       nil];
    
}

-(void)loadCheckOutTime
{
    self.checkOutTime =[[NSArray alloc]initWithObjects:@" ",
               @"12:31PM 8-14-2014",
               @" ",
               @"06:30PM 21-14-2014",
               @"",
               @"04:35PM 28-14-2014",
               @"",
               @"02:19PM 9-14-2014",
               @"",
               @"06:32PM 8-14-2014",
               nil];
    
}

-(void)loadLeadSource
{
    self.leadSourceArr =[[NSArray alloc]initWithObjects:@"Newspaper",
                        @"Adertisement",
                        @"",
                        @"Referenced",
                        @"Television",
                        @"",
                        @"",
                        @"",
                        @"Friend Reference",
                        @"",
                        nil];    
}

-(void)loadLeadChilds
{
    self.totalChilds =[[NSArray alloc]initWithObjects:@"2",
                         @"4",
                         @"",
                         @"",
                         @"3",
                         @"",
                         @"",
                         @"",
                         @"1",
                         @"",
                         nil];
}

-(void)loadLeadResonToVisit
{
    self.reasonTovisit =[[NSArray alloc]initWithObjects:@"",
                         @"",
                         @"Visit the manager",
                         @"",
                         @"",
                         @"Appointment from HR",
                         @"Employment",
                         @"Service staff",
                         @"",
                         @"",
                         nil];
}


@end

//
//  chiidAgePicker.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/28/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface childAgePicker : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property(nonatomic,copy)void (^childAgePicker)(NSString *childAge);
@end

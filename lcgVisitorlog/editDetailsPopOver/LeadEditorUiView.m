//
//  LeadEditorUiView.m
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/18/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "LeadEditorUiView.h"

@implementation LeadEditorUiView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

//-(void)drawRect:(CGRect)rect
//{
//    self.frame = CGRectMake(self.frame.size.width/2 - , self.frame.size.height/2 - 80, self.frame.size.width, self.frame.size.height);
//}

-(void)awakeFromNib{
    [self drawLineForTextField:self.firstName];
    [self drawLineForTextField:self.lastName];
    [self drawLineForTextField:self.phone];
    [self drawLineForTextField:self.email];
    [self drawLineForTextField:self.source];
    
    UITapGestureRecognizer *singleTapOnCancelView = [[UITapGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(actionForCancelImage)];
    [self.cancelImage addGestureRecognizer:singleTapOnCancelView];
    
    UITapGestureRecognizer *singleTapOnSaveView = [[UITapGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(actionForSaveImage)];
    [self.saveImage addGestureRecognizer:singleTapOnSaveView];
}


-(void)btnClick:(id)sender{ // for check out
    
    [self removeFromSuperview];
}
-(IBAction)btnCheckIn:(id)sender
{
    [self removeFromSuperview];
}

#pragma mark - action methods

-(void)actionForCancelImage{
    
    [self removeFromSuperview];
}

-(void)actionForSaveImage{
    [self removeFromSuperview];
}

-(void)sendValues:(NSString *)objValue
{
    [self.lblAddChild setFont:[UIFont fontWithName:@"Roboto-Thin" size:20.0 ]];
    //[self.lblAddChild setFont:[UIFont boldSystemFontOfSize:18.0]];
    [self.lblDetail setFont:[UIFont fontWithName:@"Roboto-Thin" size:20.0 ]];
    //[self.lblDetail setFont:[UIFont boldSystemFontOfSize:18.0]];
    [self.firstName setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.lastName setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.phone setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.email setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.lblcheckInTime setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.lblcheckOutTime setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    [self.source setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0 ]];
    
    [self.btnCheckIn.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:20.0 ]];
    [self.btnCheckOut.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:20.0 ]];
    
    

    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mma MMM-dd-YY "];
    
    
    self.firstName.text = [(Person *)objValue firstName];
    self.lastName.text = [(Person *)objValue lastName];
    self.phone.text = [(Person *)objValue phone];
    self.email.text = [(Person *)objValue email];
    
    
    self.lblcheckInTime.text = [dateFormatter stringFromDate:[(Person *)objValue checkInTime]];
    self.lblcheckOutTime.text = [dateFormatter stringFromDate:[(Person *)objValue checkOutTime]];
        
    
    if([[(Person *)objValue reasonToVisit] length] == 0){
        self.source.text = [(Person *)objValue leadSource];
    }else{
        self.source.text = [(Person *)objValue reasonToVisit];
    }
    
    
}

-(void)drawLineForTextField:(UITextField *)textfield
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,29, 200, 1.8)];
    lineView.backgroundColor = [UIColor whiteColor];
    [textfield addSubview:lineView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField==self.phone)
    {
        // All digits entered
        if (range.location == 12) {
            return NO;
        }
        
        // Reject appending non-digit characters
        if (range.length == 0 &&
            ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
            return NO;
        }
        
        // Auto-add hyphen before appending 4rd or 7th digit
        if (range.length == 0 &&
            (range.location == 3 || range.location == 7)) {
            textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
            return NO;
        }
        
        // Delete hyphen when deleting its trailing digit
        if (range.length == 1 &&
            (range.location == 4 || range.location == 8))  {
            range.location--;
            range.length = 2;
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }

    return YES;
}
@end

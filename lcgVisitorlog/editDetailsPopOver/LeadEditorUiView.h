//
//  LeadEditorUiView.h
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/18/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"


@interface LeadEditorUiView : UIView<UITextFieldDelegate>{
    NSDateFormatter *dateFormatter;
}

-(IBAction)btnClick:(id)sender;
-(IBAction)btnCheckIn:(id)sender;


@property (nonatomic,strong) IBOutlet UIImageView *cancelImage;
@property (nonatomic,strong) IBOutlet UILabel *lblDetail;
@property (nonatomic,strong) IBOutlet UILabel *lblAddChild;

@property (nonatomic,strong) IBOutlet UIImageView *saveImage;

@property (nonatomic,strong) NSMutableArray *objOnePersonCell;


@property (nonatomic,strong) IBOutlet UITextField *firstName;
@property (nonatomic,strong) IBOutlet UITextField *phone;
@property (nonatomic,strong) IBOutlet UITextField *source;
@property (nonatomic,strong) IBOutlet UITextField *lastName;
@property (nonatomic,strong) IBOutlet UITextField *email;
@property (nonatomic,strong) IBOutlet UILabel *lblcheckInTime;
@property (nonatomic,strong) IBOutlet UILabel *lblcheckOutTime;
@property (nonatomic,strong) IBOutlet UILabel *btnAddChild;

@property (nonatomic,strong) IBOutlet UIButton *btnCheckIn;
@property (nonatomic,strong) IBOutlet UIButton *btnCheckOut;


-(void)sendValues:(NSMutableArray *)objValue;
@end

//
//  CheckInOutViewController.m
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "CheckInOutViewController.h"
#import "DataCheckInOut.h"
#import "Person.h"

@interface CheckInOutViewController (){
    NSPredicate *p;
    NSArray *filteredArray;
    BOOL isAscending;
    DataCheckInOut *objDataCheckInOut;
    Person *person;
    CheckInOutTableViewCell *cell;
}

@end

@implementation CheckInOutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated
{
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mma mm-dd-yyyy"];
    
    [self.view setBackgroundColor :[UIColor colorWithRed:(26/255.0) green:(127/255.0) blue:(194/255.0) alpha:1] ];
    
    [self.tableView setBackgroundColor:[UIColor colorWithRed:(26/255.0) green:(127/255.0) blue:(194/255.0) alpha:1]];
    [self.uiViewInner setBackgroundColor:[UIColor colorWithRed:(26/255.0) green:(127/255.0) blue:(194/255.0) alpha:1] ];
    
    [self.uiViewInner.layer setCornerRadius:5.0f];
    [self.uiViewInner.layer setBorderColor:[UIColor lightTextColor].CGColor];
    [self.uiViewInner.layer setBorderWidth:1.0f];
    
    // drop shadow
    [self.uiViewInner.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.uiViewInner.layer setShadowOpacity:0.7];
    [self.uiViewInner.layer setShadowRadius:5.0];
    [self.uiViewInner.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
  
    isAscending = NO;
    
    [self setTextFieldAppearence];
    [self navigationManagement];
    [self getPersonDetails];
    
    UITapGestureRecognizer *singleTapOnSearchCancel = [[UITapGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(actionForSearchCancelImage)];
    [self.cancelImageBtn addGestureRecognizer:singleTapOnSearchCancel];
 

}

-(void)viewDidAppear:(BOOL)animated
{
    [self initialOrientationHandling];
}



- (void)viewDidLoad

{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.scrollView.contentSize=CGSizeMake(1555,1100);
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - orientation support

-(void)screenPortrait
{
    CGRect newFrame = self.view.frame;
    newFrame.origin.x = 5;
    newFrame.origin.y = 5;
    newFrame.size.width = 748;
    newFrame.size.height = 1024;
    
    [self.uiViewLeadMain setFrame:newFrame];
    [self.uiViewChild setFrame:newFrame];
    
    newFrame.origin.x = 130;
    newFrame.origin.y = 137;
    newFrame.size.width = 520;
    newFrame.size.height = 480;
    
    [self.uiViewInner setFrame:newFrame];
}
-(void)screenLandscape
{
    CGRect newFrame = self.view.frame;
    newFrame.origin.x = 5;
    newFrame.origin.y = 5;
    newFrame.size.width = 1004;
    newFrame.size.height = 728;
    
    [self.uiViewLeadMain setFrame:newFrame];
    [self.uiViewChild setFrame:newFrame];
    
    newFrame.origin.x = 240;
    newFrame.origin.y = 70;
    newFrame.size.width = 520;
    newFrame.size.height = 480;//480
    
    [self.uiViewInner setFrame:newFrame];

}

-(void)initialOrientationHandling
{
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft ||
        [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight){
        
        [self screenLandscape];
    }else if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationPortrait ||
              [[UIDevice currentDevice]orientation] == UIDeviceOrientationPortraitUpsideDown){
        
        [self screenPortrait];

    }
    else if([[UIDevice currentDevice]orientation]== UIDeviceOrientationFaceUp){
        if(self.view.frame.size.height < 900){
            [self screenLandscape];
         }else{
             [self screenPortrait];
        }
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [self initialOrientationHandling];
}

#pragma mark - scroll view
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    [self.scrollView setContentOffset: CGPointMake(10,self.scrollView.contentOffset.y)];
    // or if you are sure you wanna it always on left:
    // [aScrollView setContentOffset: CGPointMake(0, aScrollView.contentOffset.y)];
}


#pragma mark - Search box methods


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.cancelImageBtn.hidden = NO;
    NSLog(@"%@",textField.text);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.cancelImageBtn.hidden = YES;
}

-(void)actionForSearchCancelImage
{
    self.searchTextField.text = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchTerm = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self updateAsPerSearchTerm: searchTerm];
    
    return YES;
}

-(void)updateAsPerSearchTerm:(NSString *)searchTerm
{
    /*
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains[c] %@",searchTerm];
    NSArray *sortedArray = [[(Person*)self.objPersonCell firstName] filteredArrayUsingPredicate:predicate];
    //NSLog(@"%@",sortedArray);
    
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
   */
    NSRange range;
    NSMutableArray *sortedPersonObjects = [[NSMutableArray alloc]init];
    for (Person *obj in self.objPersonCell){
        range = [[obj firstName] rangeOfString:searchTerm];
        if(range.length > 0){
            [sortedPersonObjects addObject:obj];
        }
    }
    /*
    
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedPersonObjects];
    [self.tableView reloadData];
    */
}


#pragma mark - navigation bar management

-(void)titleForNavigation
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"VISITORS LOG", @"");
    [label sizeToFit];
    self.navigationItem.titleView=label;

    
   }

-(void)backBtnForNavigation
{
    UIImage *backBtnImage = [UIImage imageNamed:@"back.png"];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(actionBtnBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem  = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setLeftBarButtonItem:backItem];
    
}

-(void)doneButtonForNavigation
{
    UIImage *doneBtnImage = [UIImage imageNamed:@"large-circle.png"];

    UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [doneBtn setImage:doneBtnImage forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(actionButtonDone) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneItem  = [[UIBarButtonItem alloc] initWithCustomView:doneBtn];
    self.navigationItem.rightBarButtonItem = doneItem;
}

-(void)navigationManagement
{
    [self titleForNavigation];
    
    // change the background color to black
    //self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self backBtnForNavigation];
    [self doneButtonForNavigation];
}

                                                     
                                                     
                                                     
#pragma mark - Custom methods

-(void)actionImageBtnCheckAll{
    /*
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                      message:@"Are you sure want to Check out All ?"
                                                     delegate:self
                                            cancelButtonTitle:@"No"
                                            otherButtonTitles:@"Yes", nil];
  
    [myAlert show];
     */
}

-(void)actionImageBtnMgmtValAll{
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                      message:@"Are you sure want to Validate All ?"
                                                     delegate:self
                                            cancelButtonTitle:@"No"
                                            otherButtonTitles:@"Yes", nil];
    
    [myAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex != [alertView cancelButtonIndex]) {
        for(NSString *objStr in self.objPersonCell){
            [(Person *)objStr setImageCheckInOut:@"red-tick.png"];
        }
    }
    [self.tableView reloadData];
}

-(void)setTextFieldAppearence{
    // background1 for the textfield
//    UIImageView *searchBgImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 75, 750, 48)];
//    searchBgImage.image = [UIImage imageNamed:@"search-box.png"];
//    [self.view addSubview:searchBgImage];
//    
    // background2 for the textfield
//    UIImageView *searchBgImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(25, 80,25, 25)];
//    searchBgImage2.image = [UIImage imageNamed:@"search.png"];
//    [self.view addSubview:searchBgImage2];
//   
    
  
}
                                                     
-(void)getPersonDetails
{
    objDataCheckInOut = [[DataCheckInOut alloc]init];
    self.objPersonCell =[[NSMutableArray alloc]init];
    
    for(int i =0;i <10;i++){
        person = [[Person alloc]init];
        
        person.imageCheckInOut = [objDataCheckInOut.imageCheckInOut objectAtIndex:i];
        person.imageMgmtValidation = [objDataCheckInOut.imageMgmtValidation objectAtIndex:i];
        person.personType = [objDataCheckInOut.personType objectAtIndex:i];
        person.firstName = [objDataCheckInOut.firstNamesArr objectAtIndex:i];
        person.lastName = [objDataCheckInOut.lastNamesArr objectAtIndex:i];
        person.email = [objDataCheckInOut.emailArr objectAtIndex:i];
        person.phone = [objDataCheckInOut.phoneArr objectAtIndex:i];
        person.checkOutTime = [dateFormatter dateFromString:[objDataCheckInOut.checkOutTime objectAtIndex:i]];
        person.checkInTime = [dateFormatter dateFromString:[objDataCheckInOut.checkInTime objectAtIndex:i]];
        person.leadSource = [objDataCheckInOut.leadSourceArr objectAtIndex:i];
        person.leadTotalChilds = [objDataCheckInOut.totalChilds objectAtIndex:i];
        person.reasonToVisit = [objDataCheckInOut.reasonTovisit objectAtIndex:i];
        
        [self.objPersonCell addObject:person];
    }
    
    /*
    for(NSMutableArray *obj in self.objPersonCell){
        NSLog(@"%@  -  %@\n",obj ,[(Person *)obj personType]);
    }
     */
    
}



-(void)actionBtnBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)actionButtonDone
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -table view methods



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.objLeadEditorUIView sendValues:[self.objPersonCell objectAtIndex:indexPath.row]];
    [self.view addSubview:self.objLeadEditorUIView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CheckInOutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale=[NSLocale systemLocale];
    [dateFormat setDateFormat:@"hh:mma MM-dd-yy"];
    NSDate *currentTimeAndDate = [[NSDate alloc] init];
    
    // weakifying self
    __weak typeof(self) weakSelf = self;
    cell.blockGettingRef=^()
    {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        if(![[(Person *)[strongSelf.objPersonCell objectAtIndex:indexPath.row] imageCheckInOut]  isEqual: @"red-tick.png"])
        {
            [(Person *)[strongSelf.objPersonCell objectAtIndex:indexPath.row] setImageCheckInOut:@"red-tick.png"];
            [(Person *)[strongSelf.objPersonCell objectAtIndex:indexPath.row] setCheckOutTime:currentTimeAndDate];
            [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        }
        
    };
    
    cell.blockForMgmtVal = ^()
    {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        if(![[(Person *)[strongSelf.objPersonCell objectAtIndex:indexPath.row] imageMgmtValidation]  isEqual: @"red-tick.png"])
        {
            [(Person *)[strongSelf.objPersonCell objectAtIndex:indexPath.row] setImageMgmtValidation:@"red-tick.png"];
            [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        }
    };
    
    NSString *personType = [[NSString alloc]initWithString:[cell setValues:[self.objPersonCell objectAtIndex:indexPath.row]]];
    
    
    if([personType isEqual:@"lead"]){ // yellow
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:(231/255.0) green:(241/255.0) blue:(195/255.0) alpha:1]];
    }else if ([personType isEqual:@"visitor"]){ // greens
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:(249/255.0) green:(245/255.0) blue:(211/255.0) alpha:1]];
    }else if([personType isEqual:@"partner"]){
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:(195/255.0) green:(231/241.0) blue:(211/255.0) alpha:1]];
    }else{
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    CGRect frame = tableView.frame;
    
    // check in out button with image
    UIButton *btnImageCheckInOut = [[UIButton alloc] initWithFrame:CGRectMake(35, 20, 30, 30)];
    [btnImageCheckInOut setImage:[UIImage imageNamed:@"white-tick.png"] forState:UIControlStateNormal];
    [btnImageCheckInOut addTarget:self action:@selector(actionImageBtnCheckAll) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnCheckInOut = [[UIButton alloc] initWithFrame:CGRectMake(60, 20, 60, 30)];
    [btnCheckInOut setTitle:@"In/Out" forState:UIControlStateNormal];
    [btnCheckInOut addTarget:self action:@selector(actionBtnCheckInOut) forControlEvents:UIControlEventTouchUpInside];
    [btnCheckInOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckInOut.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnCheckInOut.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    
    UIButton *btnImageCheckInOutSort = [[UIButton alloc] initWithFrame:CGRectMake(114, 27, 20, 20)];
    [btnImageCheckInOutSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnName = [[UIButton alloc] initWithFrame:CGRectMake(140, 20, 80, 30)];
    [btnName setTitle:@"Name" forState:UIControlStateNormal];
    [btnName addTarget:self action:@selector(actionBtnName) forControlEvents:UIControlEventTouchUpInside];    [btnName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnName.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnName.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    
    UIButton *btnImageNameSort = [[UIButton alloc] initWithFrame:CGRectMake(203, 27, 20, 20)];
    [btnImageNameSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnEmail = [[UIButton alloc] initWithFrame:CGRectMake(290, 20, 100, 30)];
    [btnEmail setTitle:@"Email" forState:UIControlStateNormal];
    [btnEmail addTarget:self action:@selector(actionBtnEmail) forControlEvents:UIControlEventTouchUpInside];
    [btnEmail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEmail.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnEmail.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    
    
    UIButton *btnImageEmailSort = [[UIButton alloc] initWithFrame:CGRectMake(362, 27, 20, 20)];
    [btnImageEmailSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnCheckInOutTime = [[UIButton alloc] initWithFrame:CGRectMake(475, 20, 160, 30)];
    [btnCheckInOutTime setTitle:@"Time:In/Out" forState:UIControlStateNormal];
    [btnCheckInOutTime addTarget:self action:@selector(actionBtnCheckInOutTime) forControlEvents:UIControlEventTouchUpInside];
    [btnCheckInOutTime setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCheckInOutTime.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnCheckInOutTime.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    
    UIButton *btnImageTimeSort = [[UIButton alloc] initWithFrame:CGRectMake(602, 27, 20, 20)];
    [btnImageTimeSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnImageMgmtVal = [[UIButton alloc] initWithFrame:CGRectMake(625, 20, 30, 30)];
    [btnImageMgmtVal setImage:[UIImage imageNamed:@"white-tick.png"] forState:UIControlStateNormal];
    [btnImageMgmtVal addTarget:self action:@selector(actionImageBtnMgmtValAll) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnMgmtValidation = [[UIButton alloc] initWithFrame:CGRectMake(653, 20, 80, 30)];
    [btnMgmtValidation setTitle:@"Mgmt-val" forState:UIControlStateNormal];
    [btnMgmtValidation addTarget:self action:@selector(actionBtnManagementValidation) forControlEvents:UIControlEventTouchUpInside];
    [btnMgmtValidation setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnMgmtValidation.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnMgmtValidation.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    UIButton *btnImageMgmtSort = [[UIButton alloc] initWithFrame:CGRectMake(730, 27, 20, 20)];
    [btnImageMgmtSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnPhone = [[UIButton alloc] initWithFrame:CGRectMake(770, 20, 100, 30)];
    [btnPhone setTitle:@"Phone" forState:UIControlStateNormal];
    [btnPhone addTarget:self action:@selector(actionBtnContactNumber) forControlEvents:UIControlEventTouchUpInside];
    [btnPhone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPhone.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnPhone.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    UIButton *btnPhoneSort = [[UIButton alloc] initWithFrame:CGRectMake(845, 27, 20, 20)];
    [btnPhoneSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];
    
    UIButton *btnLeadSource = [[UIButton alloc] initWithFrame:CGRectMake(950, 20, 100, 30)];
    [btnLeadSource setTitle:@"Source" forState:UIControlStateNormal];
    [btnLeadSource setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLeadSource addTarget:self action:@selector(actionBtnLeadSource) forControlEvents:UIControlEventTouchUpInside];
    [btnLeadSource.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnLeadSource.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    UIButton *btnLeadSort = [[UIButton alloc] initWithFrame:CGRectMake(1029, 27, 20, 20)];
    [btnLeadSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];

    
    UIButton *btnLeadChilds = [[UIButton alloc] initWithFrame:CGRectMake(1130, 20, 100, 30)];
    [btnLeadChilds setTitle:@"Children" forState:UIControlStateNormal];
    [btnLeadChilds setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLeadChilds addTarget:self action:@selector(actionBtnLeadChilds) forControlEvents:UIControlEventTouchUpInside];
    [btnLeadChilds.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnLeadChilds.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    
    UIButton *btnLeadChildSort = [[UIButton alloc] initWithFrame:CGRectMake(1214, 27, 20, 20)];
    [btnLeadChildSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];

    UIButton *btnReasonToVisit = [[UIButton alloc] initWithFrame:CGRectMake(1240, 20, 150, 30)];
    [btnReasonToVisit setTitle:@"Reason for visit" forState:UIControlStateNormal];
    [btnReasonToVisit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnReasonToVisit addTarget:self action:@selector(actionBtnReasonTovisit) forControlEvents:UIControlEventTouchUpInside];
    [btnReasonToVisit.titleLabel setFont:[UIFont fontWithName:@"Roboto-Thin" size:18.0]];
    [btnReasonToVisit.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];

    UIButton *btnResasonToVisitSort = [[UIButton alloc] initWithFrame:CGRectMake    (1380, 27, 20, 20)];
    [btnResasonToVisitSort setImage:[UIImage imageNamed:@"down-arrow-white.png"] forState:UIControlStateNormal];

    
    // background header view
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    headerView.backgroundColor = [UIColor blackColor];
    
    
    [headerView addSubview:btnLeadChildSort];
    [headerView addSubview:btnResasonToVisitSort];
    [headerView addSubview:btnLeadSort];
    [headerView addSubview:btnPhoneSort];
    [headerView addSubview:btnImageMgmtSort];
    [headerView addSubview:btnImageTimeSort];
    [headerView addSubview:btnImageEmailSort];
    [headerView addSubview:btnImageNameSort];
    [headerView addSubview:btnImageCheckInOutSort];
    [headerView addSubview:btnImageCheckInOut];
    [headerView addSubview:btnImageMgmtVal];
    [headerView addSubview:btnCheckInOut];
    [headerView addSubview:btnName];
    [headerView addSubview:btnEmail];
    [headerView addSubview:btnCheckInOutTime];
    [headerView addSubview:btnPhone];
    [headerView addSubview:btnLeadSource];
    [headerView addSubview:btnLeadChilds];
    [headerView addSubview:btnReasonToVisit];
    [headerView addSubview:btnMgmtValidation];
    return headerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"title one";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 65.0f;
}

#pragma mark - actions of header buttons 

-(void)actionBtnCheckInOut
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a imageCheckInOut];
            NSString *second = [(Person*)b imageCheckInOut];
            return [first  compare:second];
        }];
        isAscending = YES;
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a imageCheckInOut];
            NSString *second = [(Person*)b imageCheckInOut];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnCheckInOutTime
{
    
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSDate *first = [(Person*)a checkInTime];
            NSDate *second = [(Person*)b checkInTime];
            return [first  compare:second];
        }];
        isAscending = YES;
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSDate *first = [(Person*)a checkInTime];
            NSDate *second = [(Person*)b checkInTime];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
    
}

-(void)actionBtnName
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a firstName];
            NSString *second = [(Person*)b firstName];
            return [first  compare:second];
        }];
        isAscending = YES;
   
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a firstName];
            NSString *second = [(Person*)b firstName];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
}

-(void)actionBtnEmail
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a email];
            NSString *second = [(Person*)b email];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a email];
            NSString *second = [(Person*)b email];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnContactNumber
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a phone];
            NSString *second = [(Person*)b phone];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a phone];
            NSString *second = [(Person*)b phone];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnManagementValidation
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a imageMgmtValidation];
            NSString *second = [(Person*)b imageMgmtValidation];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a imageMgmtValidation];
            NSString *second = [(Person*)b imageMgmtValidation];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnLeadSource
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a leadSource];
            NSString *second = [(Person*)b leadSource];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a leadSource];
            NSString *second = [(Person*)b leadSource];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnLeadChilds
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a leadTotalChilds];
            NSString *second = [(Person*)b leadTotalChilds];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a leadTotalChilds];
            NSString *second = [(Person*)b leadTotalChilds];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}

-(void)actionBtnReasonTovisit
{
    NSArray *sortedArray;
    if(!isAscending){
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a reasonToVisit];
            NSString *second = [(Person*)b reasonToVisit];
            return [first  compare:second];
        }];
        isAscending = YES;
        
    }else{
        sortedArray = [self.objPersonCell sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Person*)a reasonToVisit];
            NSString *second = [(Person*)b reasonToVisit];
            return [second compare:first];
        }];
        isAscending = NO;
    }
    [self.objPersonCell removeAllObjects];
    [self.objPersonCell addObjectsFromArray:sortedArray];
    [self.tableView reloadData];

}


- (IBAction)viewBtnAction:(id)sender {
}
@end

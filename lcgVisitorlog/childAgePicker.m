//
//  chiidAgePicker.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/28/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "childAgePicker.h"

@interface childAgePicker ()

@end

@implementation childAgePicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPickerView *pv = [[UIPickerView alloc] init];
    [pv setShowsSelectionIndicator:YES];
    [pv setDataSource:self];
    [pv setDelegate:self];
    
    [self.view addSubview:pv];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 14;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(row==0)
    {
   return @"-";
    }
    else{
        NSString *childAge1 =[NSString stringWithFormat:@"%d",row+2];
        return childAge1;
        
    }
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
     NSString *childAge1 =[NSString stringWithFormat:@"%d",row+2];
    if(row==0)
    {
        self.childAgePicker(@"-");
    }
    else
        
     self.childAgePicker(childAge1);

}
@end

//
//  DataCheckInOut.h
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataCheckInOut : NSObject

@property (nonatomic,strong)    NSMutableArray *imageCheckInOut;
@property (nonatomic,strong)    NSArray *firstNamesArr;
@property (nonatomic,strong)    NSArray *lastNamesArr;
@property (nonatomic,strong)    NSArray *phoneArr;
@property (nonatomic,strong)    NSArray *emailArr;
@property (nonatomic,strong)    NSArray *checkInTime;
@property (nonatomic,strong)    NSArray *checkOutTime;
@property (nonatomic,strong)    NSArray *personType;
@property (nonatomic,strong)    NSMutableArray *imageMgmtValidation;
@property (nonatomic,strong) NSArray *leadSourceArr;
@property (nonatomic,strong) NSArray *totalChilds;
@property (nonatomic,strong) NSArray *reasonTovisit;

@property (nonatomic,strong) NSObject *objOfRows;

@end

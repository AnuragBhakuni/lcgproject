//
//  MainViewController.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/16/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "MainViewController.h"
#import "checkInouttypeTableViewController.h"
#import "leadSourceTableViewController.h"
#import "reasonForvisitTableViewController.h"
#import "CheckInOutViewController.h"
#import "SettingScreenViewController.h"
#import "childAgePicker.h"

@interface MainViewController ()


//all other properties
@property ( nonatomic) NSInteger addNumberofchild;
@property ( nonatomic) NSInteger restrictScrollFrame;
@property ( nonatomic) NSInteger flagChangingviewposition;
@property ( nonatomic) NSInteger textFieldposition;
@property ( nonatomic) NSInteger textFieldpositionfororientation;
@property(nonatomic)NSMutableArray *childNameObj;
@property(nonatomic)NSMutableArray *childAgeObj;
@property ( nonatomic) NSInteger childNametag;
@property ( nonatomic) NSInteger childAgetag;
@property ( nonatomic) NSInteger contentHeightPotrait;
@property ( nonatomic) NSInteger contentHeightLandscape;


// all buttons
@property (strong, nonatomic)  UIButton *btnSetting;
@property (strong, nonatomic)  UIButton *btnManagement;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckinouttype;
@property (strong, nonatomic) IBOutlet UIButton *btnReasonforvisit;
@property (strong, nonatomic) IBOutlet UIButton *btnNewchild;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckin;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckout;
@property (strong, nonatomic) IBOutlet UIButton *btnReset;
@property (strong, nonatomic) IBOutlet UIButton *btnRemovechild;






- (IBAction)btnCheckin:(id)sender;
- (IBAction)btnCheckout:(id)sender;


//popover views
@property (nonatomic,strong) UIPopoverController *popOver;




//other views
@property (strong, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (strong, nonatomic) IBOutlet UIView *viewUppersection;
//@property (strong, nonatomic) IBOutlet UIView *viewLowersection;
@property (strong, nonatomic) IBOutlet UIView *viewForbackgroundcolor;
@property (strong, nonatomic) IBOutlet UIImageView *viewImagebackground;
@property (strong, nonatomic) IBOutlet UIImageView *viewsearchpng;
@property (strong, nonatomic) IBOutlet UIImageView *viewSearchbox;

@property (strong, nonatomic) IBOutlet UIImageView *viewCrossimage;

@property (strong, nonatomic) IBOutlet UIPickerView *viewPickerage;


// all IBAction
- (IBAction)actionBtncheckintype:(UIButton *)sender;
- (IBAction)actionBtnshowcontent:(id)sender;
- (IBAction)actionBtnaddchild:(id)sender;
- (IBAction)actionResetalltextfield:(id)sender;
- (IBAction)actionRemovechild:(id)sender;

- (IBAction)childAgePopOver:(id)sender;



//all labels
@property (strong, nonatomic) IBOutlet UILabel *lblHeaderinlowerview;


//all textfields
@property (strong, nonatomic) IBOutlet UITextField *txtParentlastname;
@property (strong, nonatomic) IBOutlet UITextField *txtParentfirstname;
@property (strong, nonatomic) IBOutlet UITextField *txtPhonenumber;
@property (strong, nonatomic) IBOutlet UITextField *txtEmailaddress;
@property (strong, nonatomic) IBOutlet UITextField *txtChildname;
@property (strong, nonatomic) IBOutlet UITextField *txtChildage;
@property (strong, nonatomic) IBOutlet UITextField *txtReasonforvisit;
@property (strong, nonatomic) IBOutlet UITextField *txtSearchbar;
@end

@implementation MainViewController

//- (BOOL)disablesAutomaticKeyboardDismissal
//{
//    return NO;
//}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.childAgeObj =[[NSMutableArray alloc]init];
    self.childNameObj =[[NSMutableArray alloc]init];
    self.addNumberofchild=1;
    self.restrictScrollFrame=1;
    self.textFieldposition=253;
    self.flagChangingviewposition=1;
    self.childNametag=100;
    self.childAgetag=200;
    self.contentHeightPotrait=360;
    self.contentHeightLandscape=288;
    self.view.backgroundColor =[UIColor colorWithRed:26/255.0f green:127/255.0f blue:194/255.0f alpha:1.0f];
      self.viewForbackgroundcolor.backgroundColor =[UIColor colorWithRed:26/255.0f green:127/255.0f blue:194/255.0f alpha:1.0f];
    self.btnCheckinouttype.backgroundColor =[UIColor colorWithRed:245/255.0f green:208/255.0f blue:112/255.0f alpha:1.0f];
    self.btnCheckin.backgroundColor =[UIColor colorWithRed:128/255.0f green:245/255.0f blue:112/255.0f alpha:1.0f];
    self.btnCheckout.backgroundColor =[UIColor colorWithRed:234/255.0f green:83/255.0f blue:38/255.0f alpha:1.0f];
    [self.btnCheckinouttype setImage:[UIImage imageNamed:@"down-arrow.png"] forState:UIControlStateNormal];
    self.btnCheckinouttype.titleEdgeInsets = UIEdgeInsetsMake(0, -self.btnCheckinouttype.imageView.frame.size.width, 0, self.btnCheckinouttype.imageView.frame.size.width);
    self.btnCheckinouttype.imageEdgeInsets = UIEdgeInsetsMake(5,self.btnCheckinouttype.titleLabel.frame.size.width +54, 0, -self.btnCheckinouttype.titleLabel.frame.size.width);
    [self.btnNewchild setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    self.btnNewchild.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    self.btnNewchild.imageEdgeInsets = UIEdgeInsetsMake(0,20, 1, 100);
    
    
    

    //styling remove child button
    self.btnRemovechild.layer.cornerRadius=(self.btnRemovechild.frame.size.width)/2;
    self.btnRemovechild.layer.borderWidth=2;
    self.btnRemovechild.layer.borderColor=(__bridge CGColorRef)([UIColor whiteColor]);
    
    UIView *lineViewinbutton = [[UIView alloc] initWithFrame:CGRectMake(10,self.btnRemovechild.layer.cornerRadius, self.btnRemovechild.layer.cornerRadius, 1.8)];
    lineViewinbutton.backgroundColor = [UIColor blackColor];
    [self.btnRemovechild addSubview:lineViewinbutton];
    
    //call function to make round button
    
    [self makeButtonround:self.btnCheckinouttype];
    [self makeButtonround:self.btnCheckin];
    [self makeButtonround:self.btnCheckout];
    [self makeButtonround:self.btnReset];

    
    //call function to draw line below text field
   
    [self drawLine:self.txtParentfirstname];
    [self drawLine:self.txtParentlastname];
    [self drawLine:self.txtPhonenumber];
    [self drawLine:self.txtReasonforvisit];
    [self drawLine:self.txtChildage];
    [self drawLine:self.txtChildname];
    [self drawLine:self.txtEmailaddress];
   
   //call function to change placeholder style
    [self changePlaceholdercolor:self.txtParentfirstname];
    [self changePlaceholdercolor:self.txtParentlastname];
    [self changePlaceholdercolor:self.txtPhonenumber];
    [self changePlaceholdercolor:self.txtReasonforvisit];
    [self changePlaceholdercolor:self.txtChildage];
    [self changePlaceholdercolor:self.txtChildname];
    [self changePlaceholdercolor:self.txtEmailaddress];
    [self changePlaceholdercolor:self.txtSearchbar];
    
    
 
    
    //change text color
    self.txtParentfirstname.textColor=[UIColor whiteColor];
    self.txtParentlastname.textColor=[UIColor whiteColor];
    self.txtPhonenumber.textColor=[UIColor whiteColor];
    self.txtReasonforvisit.textColor=[UIColor whiteColor];
    self.txtChildage.textColor=[UIColor whiteColor];
    self.txtChildname.textColor=[UIColor whiteColor];
    self.txtEmailaddress.textColor=[UIColor whiteColor];
    
    //change color of cursor
    [self changeCursorcolor:self.txtParentfirstname];
    [self changeCursorcolor:self.txtParentlastname];
    [self changeCursorcolor:self.txtPhonenumber];
    [self changeCursorcolor:self.txtReasonforvisit];
    [self changeCursorcolor:self.txtChildage];
    [self changeCursorcolor:self.txtChildname];
    [self changeCursorcolor:self.txtEmailaddress];
    
    //change font textfield
    [self changeFont:self.txtParentfirstname];
    [self changeFont:self.txtParentlastname];
    [self changeFont:self.txtPhonenumber];
    [self changeFont:self.txtReasonforvisit];
    [self changeFont:self.txtChildage];
    [self changeFont:self.txtChildname];
    [self changeFont:self.txtEmailaddress];
    [self changeFont:self.txtSearchbar];
    
    //change font label
    [self changeFontLowerSectionHeader:self.lblHeaderinlowerview];
    
    //change font button
    
    [self changeButtonFont:self.btnReset];
    [self changeButtonFont:self.btnCheckin];
    [self changeButtonFont:self.btnCheckout];
    [self changeButtonFont:self.btnCheckinouttype];
    [self changeButtonFont:self.btnNewchild];
    
    
    

  // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)viewWillAppear:(BOOL)animated
{
    
    //Customize your navigator View with navigator title and  buttons.
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"VISITORS LOG", @"");
    [label sizeToFit];
    self.navigationItem.titleView=label;
    self.btnSetting =  [[UIButton alloc]initWithFrame:CGRectMake(0, -3, 30, 30)];
    [self.btnSetting setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
    [self.btnSetting addTarget:self action:@selector(settingScreen) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIBarButtonItem *barButtonsetting = [[UIBarButtonItem alloc] initWithCustomView:self.btnSetting];
    
    self.btnManagement =  [[UIButton alloc]initWithFrame:CGRectMake(10, -80, 30, 30)];
    [self.btnManagement setImage:[UIImage imageNamed:@"managementvalidation.png"] forState:UIControlStateNormal];
    [self.btnManagement addTarget:self action:@selector(checkAndManagementScreen) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *barButtonmanagementvalidation = [[UIBarButtonItem alloc] initWithCustomView:self.btnManagement];
    NSArray *barButtonArray = [NSArray arrayWithObjects:barButtonsetting,  barButtonmanagementvalidation , nil];
    self.navigationItem.rightBarButtonItems = barButtonArray;
    [self.viewScroll setScrollEnabled:NO];
    
   
    
    [self.viewCrossimage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteCharatcter)];
    [singleTap setNumberOfTapsRequired:1];
   [self.viewCrossimage addGestureRecognizer:singleTap];
    
    
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation) )
    {
       
        
        self.viewImagebackground.frame=CGRectMake(0, 0, 768, 650);
        self.viewForbackgroundcolor.frame=CGRectMake(0,650, 768, 446);
        self.viewUppersection.frame=CGRectMake(0,0, 768, 349);
        self.btnCheckinouttype.frame=CGRectMake(126,240, 146, 58);
        self.viewSearchbox.frame=CGRectMake(318,240, 343, 58);
        self.viewsearchpng.frame=CGRectMake(329,255, 26, 30);
        self.txtSearchbar.frame=CGRectMake(363,248, 290, 44);
        self.lblHeaderinlowerview.frame=CGRectMake(195,353, 378, 28);
        self.viewScroll.frame=CGRectMake(0 ,448, 768, 360);
        
        self.btnReset.frame=CGRectMake(139 ,167, 139, 56);
        self.btnCheckin.frame=CGRectMake(315 ,167, 139, 56);
        self.btnCheckout.frame=CGRectMake(494 ,167, 139, 56);
        
        
        self.txtParentlastname.frame=CGRectMake(447 ,16, 295, 30);
        self.txtEmailaddress.frame=CGRectMake(447 ,89, 295, 30);
        self.txtReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
        self.txtChildage.frame=CGRectMake(447 ,253, 295, 30);
        
        self.btnReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
        self.btnRemovechild.frame=CGRectMake(649 ,198, 40, 40);
        self.viewCrossimage.frame=CGRectMake(628, 257, 25, 25);
        
        self.viewScroll.contentSize=CGSizeMake(self.viewScroll.frame.size.width, self.viewScroll.frame.size.height);

        
    }
    else if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) )
    {
       
        
        self.viewImagebackground.frame=CGRectMake(0, 0, 1024, 630);
        self.viewForbackgroundcolor.frame=CGRectMake(0, 630, 1024, 138);
        self.viewUppersection.frame=CGRectMake(0,0, 1024, 320);
     
        self.btnCheckinouttype.frame=CGRectMake(156,170, 146, 58);
        self.viewSearchbox.frame=CGRectMake(400,170, 543, 58);
        self.viewsearchpng.frame=CGRectMake(411,185, 26, 30);
        self.txtSearchbar.frame=CGRectMake(445,178, 490, 44);
        self.lblHeaderinlowerview.frame=CGRectMake(325,255, 378, 28);
        
         self.viewScroll.frame=CGRectMake(0 ,290, 1024, 288);
        
        
       
        self.btnReset.frame=CGRectMake(149 ,1, 189, 56);
        self.btnCheckin.frame=CGRectMake(385 ,1, 189, 56);
        self.btnCheckout.frame=CGRectMake(621 ,1, 189, 56);

        
        self.txtParentlastname.frame=CGRectMake(647 ,16, 295, 30);
        self.txtEmailaddress.frame=CGRectMake(647 ,89, 295, 30);
        self.txtReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
        self.txtChildage.frame=CGRectMake(647 ,253, 295, 30);
        
        self.btnReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
        self.btnRemovechild.frame=CGRectMake(899 ,198, 40, 40);
         self.viewCrossimage.frame=CGRectMake(910, 187, 25, 25);
         self.viewScroll.contentSize=CGSizeMake(self.viewScroll.frame.size.width   , self.viewScroll.frame.size.height);

        
        
    }
    else if (UIDeviceOrientationFaceUp) {
       if(self.view.frame.size.height>900)
       {
           
           self.viewImagebackground.frame=CGRectMake(0, 0, 768, 650);
           self.viewForbackgroundcolor.frame=CGRectMake(0, 650, 768, 446);
            self.viewUppersection.frame=CGRectMake(0,0, 768, 349);
           self.btnCheckinouttype.frame=CGRectMake(126,240, 146, 58);
           
           self.viewSearchbox.frame=CGRectMake(318,240, 343, 58);
           self.viewsearchpng.frame=CGRectMake(329,255, 26, 30);
           self.txtSearchbar.frame=CGRectMake(363,248, 290, 44);
           self.lblHeaderinlowerview.frame=CGRectMake(195,353, 378, 28);
           
            self.viewScroll.frame=CGRectMake(0 ,448, 768, 360);
           
           self.btnReset.frame=CGRectMake(139 ,167, 139, 56);
           self.btnCheckin.frame=CGRectMake(315 ,167, 139, 56);
           self.btnCheckout.frame=CGRectMake(494 ,167, 139, 56);

           
           self.txtParentlastname.frame=CGRectMake(447 ,16, 295, 30);
           self.txtEmailaddress.frame=CGRectMake(447 ,89, 295, 30);
           self.txtReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
           self.txtChildage.frame=CGRectMake(447 ,253, 295, 30);
           
           self.btnReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
           self.btnRemovechild.frame=CGRectMake(649 ,198, 40, 40);
            self.viewCrossimage.frame=CGRectMake(628, 257, 25, 25);
            self.viewScroll.contentSize=CGSizeMake(self.viewScroll.frame.size.width, self.viewScroll.frame.size.height);
           
            }
        else
        {
            
            self.viewImagebackground.frame=CGRectMake(0, 0, 1024, 630);
            self.viewForbackgroundcolor.frame=CGRectMake(0, 630, 1024, 138);
          self.viewUppersection.frame=CGRectMake(0,0, 1024, 320);
            
            self.btnCheckinouttype.frame=CGRectMake(156,170, 146, 58);
            self.viewSearchbox.frame=CGRectMake(400,170, 543, 58);
            self.viewsearchpng.frame=CGRectMake(411,185, 26, 30);
            self.txtSearchbar.frame=CGRectMake(445,178, 490, 44);
            self.lblHeaderinlowerview.frame=CGRectMake(325,255, 378, 28);

            
             self.viewScroll.frame=CGRectMake(0 ,290, 1024, 288);
            
            
            self.btnReset.frame=CGRectMake(149 ,1, 189, 56);
            self.btnCheckin.frame=CGRectMake(385 ,1, 189, 56);
            self.btnCheckout.frame=CGRectMake(621 ,1, 189, 56);
            
            
            self.txtParentlastname.frame=CGRectMake(647 ,16, 295, 30);
            self.txtEmailaddress.frame=CGRectMake(647 ,89, 295, 30);
            self.txtReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
            self.txtChildage.frame=CGRectMake(647 ,253, 295, 30);
            
            self.btnReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
            self.btnRemovechild.frame=CGRectMake(899 ,198, 40, 40);
             self.viewCrossimage.frame=CGRectMake(910, 187, 25, 25);
             self.viewScroll.contentSize=CGSizeMake(self.viewScroll.frame.size.width, self.viewScroll.frame.size.height);

           

        }
    }
    
    
  
    
}


-(void)deleteCharatcter
{
    if(self.txtSearchbar.text.length!=0 )
    {
        self.txtSearchbar.text = @ " ";
        [self.viewCrossimage setHidden:YES];
    }
    else
    {
     [self.viewCrossimage setHidden:NO];
    }
    
    
    }

-(void)changeImage:(UIImage *)image xposition:(NSInteger *)x ypostion:(NSInteger *)y width:(NSInteger *)width height:(NSInteger *)height
{
    
}

-(void)changeButtonFont:(UIButton *)button
{
    if(button.tag==400)
    {
    button.titleLabel.font=[UIFont fontWithName:@"Roboto-Light" size:26];
    }
    else
    {
    button.titleLabel.font=[UIFont fontWithName:@"Roboto-Thin" size:20];
    }

}

//- (BOOL)validateEmailWithString:(NSString*)email
//{
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:email];
//}



-(void)changeFontLowerSectionHeader:(UILabel *)label
{
    label.font=[UIFont fontWithName:@"Roboto-Light" size:26];
}

-(void)changeFont:(UITextField *)textField
{
    if(textField.tag==300)
    {
        textField.font=[UIFont fontWithName:@"Roboto-Light" size:24];
    }
    else
    {
   textField.font=[UIFont fontWithName:@"Roboto-Thin" size:20];
    }
}

#pragma mark:-change cursor color

-(void)changeCursorcolor:(UITextField *)textField
{
[[textField valueForKey:@"textInputTraits"] setValue:[UIColor whiteColor] forKey:@"insertionPointColor"];
}

#pragma mark:- Make button round
-(void)makeButtonround:(UIButton *)button
{
    button.layer.cornerRadius = 5;
  
}

#pragma mark:- Change style of particular placeholder
-(void)changePlaceholdercolor:(UITextField *)textField
{
    if(textField.tag==300)
    {
        UIColor *color = [UIColor lightGrayColor];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder  attributes:@{NSForegroundColorAttributeName: color}];
    }
    else
    {
    UIColor *color = [UIColor whiteColor];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder  attributes:@{NSForegroundColorAttributeName: color}];
    }
}

#pragma mark:- Draw line below textfield
-(void)drawLine:(UITextField *)textField
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,29, self.view.frame.size.width, 1.8)];
    lineView.backgroundColor = [UIColor whiteColor];
    [textField addSubview:lineView];
    
    
}

#pragma mark:-pushing checkout and mangaement screen
-(void)checkAndManagementScreen
{
    CheckInOutViewController *objCheckInOutViewController=[[CheckInOutViewController alloc]initWithNibName:@"CheckInOutViewController" bundle:nil];
     [self.navigationController pushViewController:objCheckInOutViewController animated:YES];
    
}

#pragma mark:-pushing setting screen
-(void)settingScreen
{
    
    SettingScreenViewController *objSettingScreenViewController=[[SettingScreenViewController alloc]initWithNibName:@"SettingScreenViewController" bundle:nil];
    [self.navigationController pushViewController:objSettingScreenViewController animated:YES];
    

}
- (void)keyboardDismiss
{
 [self.view endEditing:YES];

}

#pragma mark:- text Feild Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField ==self.txtChildage)
    {
        //[self.txtChildage resignFirstResponder];
        [self   keyboardDismiss];
        [self   childAgePopOver:self.txtChildage];
        
    }
    
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         
         if(textField==x)
         {
             // [x resignFirstResponder];
             //  [self.view endEditing:YES];
             [self   keyboardDismiss];
             [self childAgePopOver:x];
         }
     }];
    return  YES;
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
   
    if(textField == self.txtSearchbar)
    {
        [self.viewCrossimage setHidden:NO];
        
    }
    if(textField ==self.txtChildage)
    {
        [self.txtChildage resignFirstResponder];
        
        
    }
    
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         
         if(textField==x)
         {
             [x resignFirstResponder];
            
             
         }
     }];
  
    
    
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        
        self.flagChangingviewposition=1;
        
       
        if(textField==self.txtChildname ||textField==self.txtReasonforvisit)
        {
          //  [self.viewScroll setContentOffset:CGPointMake(0, 130)];
            [self setViewMovedUp:YES];
            
            
        }
        
//        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//         {
//             
//             if(textField==x)
//             {
//                // [self.viewScroll setContentOffset:CGPointMake(0, 290)];
//                 [self setViewMovedUp:YES];
//             }
//         }];
        
        [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
         {
             
             if(textField==x)
             {
                // [self.viewScroll setContentOffset:CGPointMake(0, 290)];
                 [self setViewMovedUp:YES];
             }
         }];
        
        
        if(textField==self.txtReasonforvisit)
        {
            [self.btnReasonforvisit setHidden:YES];
        }
        
    }
    
   else if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        self.flagChangingviewposition=2;
        if(textField==self.txtChildname || textField==self.txtReasonforvisit ||textField==self.txtPhonenumber||textField==self.txtEmailaddress )
        {
            
//            [UIView animateWithDuration:0.3 animations:^
//             {
//                 CGRect rect = self.view.frame;
//                 self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
                 
                 [self setViewMovedUp2:YES];
                 
//             }];
            
            
        }
        
        if (textField==self.txtParentfirstname || textField ==self.txtParentlastname) {
            
//            [UIView animateWithDuration:0.3 animations:^
//             {
//                 CGRect rect = self.view.frame;
//                 self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -280 , rect.size.width, rect.size.height);
                 
                 
                   [self setViewMovedUp2:YES];
                 
//             }];
            
            
        }
        
//        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//         {
//             
//             if(textField==x)
//             {
////                 [UIView animateWithDuration:0.3 animations:^
////                  {
////                      CGRect rect = self.view.frame;
////                      self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
//                        [self setViewMovedUp2:YES];
//                      
////                  }];
//             }
//         }];
        
        
        [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
         {
             
             if(textField==x)
             {
//                 [UIView animateWithDuration:0.3 animations:^
//                  {
//                      CGRect rect = self.view.frame;
//                      self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
  [self setViewMovedUp2:YES];
//                  }];
             }
         }];
        
        
        
        if(textField==self.txtReasonforvisit)
        {
            [self.btnReasonforvisit setHidden:YES];
            
            
        }
        
    }

  else  if(UIDeviceOrientationFaceUp)
    {
        if(self.view.frame.size.height>900)
        {
            
            self.flagChangingviewposition=1;
            if(textField==self.txtChildname ||textField==self.txtReasonforvisit)
            {
              //  [self.viewScroll setContentOffset:CGPointMake(0, 130)];
                [self setViewMovedUp:YES];
            }
            
//            [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//             {
//                 
//                 if(textField==x)
//                 {
//                    // [self.viewScroll setContentOffset:CGPointMake(0, 290)];
//                     [self setViewMovedUp:YES];
//                 }
//             }];
            
            [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
             {
                 
                 if(textField==x)
                 {
                     //[self.viewScroll setContentOffset:CGPointMake(0, 290)];
                     [self setViewMovedUp:YES];
                 }
             }];
            
            
            if(textField==self.txtReasonforvisit)
            {
                [self.btnReasonforvisit setHidden:YES];
            }
        }
       else
       {
          self.flagChangingviewposition=2;
          if(textField==self.txtChildname ||textField==self.txtReasonforvisit ||textField==self.txtPhonenumber||textField==self.txtEmailaddress )
          {
              
//              [UIView animateWithDuration:0.3 animations:^
//               {
//                   CGRect rect = self.view.frame;
//                   self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
  [self setViewMovedUp2:YES];
//               }];
              
              
          }
          
          if (textField==self.txtParentfirstname || textField ==self.txtParentlastname) {
              
//              [UIView animateWithDuration:0.3 animations:^
//              {
//                   CGRect rect = self.view.frame;
//                   self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -230 , rect.size.width, rect.size.height);
                 [self setViewMovedUp2:YES];
               
//               }];
              
              
          }
          
//          [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//           {
//               
//               if(textField==x)
//               {
////                   [UIView animateWithDuration:0.3 animations:^
////                    {
////                        CGRect rect = self.view.frame;
////                        self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
//                      [self setViewMovedUp2:YES];
////                    
////                    }];
//               }
//           }];
          
          
          [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
           {
               
               if(textField==x)
               {
//                   [UIView animateWithDuration:0.3 animations:^
//                    {
//                        CGRect rect = self.view.frame;
//                        self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -330 , rect.size.width, rect.size.height);
                          [self setViewMovedUp2:YES];
                        
//                    }];
               }
           }];
          
          
          
          if(textField==self.txtReasonforvisit)
          {
              [self.btnReasonforvisit setHidden:YES];
              
              
          }
          
           
       }
        
        
    }
    
    //return YES;
}


- (void)setViewMovedUp2:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    // Make changes to the view's frame inside the animation block. They will be animated instead
    // of taking place immediately.
    __block CGRect rect = self.view.frame;
    if (movedUp)
    {
       
       
        [UIView animateWithDuration:12 animations:^
                       {
        rect.origin.y -= 230;
                       }];
    
    }
    else
    {
        [UIView animateWithDuration:12 animations:^
         {
             rect.origin.y += 230;
         }];
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}
- (void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    // Make changes to the view's frame inside the animation block. They will be animated instead
    // of taking place immediately.
    CGRect rect = self.view.frame;
    if (movedUp)
    {
       rect.origin.y -= 75;
      
    }
    else
    {
        rect.origin.y += 75;
       
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}




- (void)textFieldDidEndEditing:(UITextField *)textField{
     [self.viewScroll setScrollEnabled:YES];
  //  [textField resignFirstResponder];
    if(textField == self.txtSearchbar && self.txtSearchbar.text.length==0)
    {
        [self.viewCrossimage setHidden:YES];
        
    }
    
    if(textField ==self.txtChildage)
    {
        [self.viewPickerage setHidden:YES];
        
    }
    
    if(textField ==self.txtEmailaddress)
    {
//        if([self validateEmailWithString:self.txtEmailaddress.text] || self.txtEmailaddress.text.length==0)
//        {
//            
//        }
//        else
//        {
//            
//            UIAlertView *alertEmail = [[UIAlertView alloc]initWithTitle:@"Wrong Email Pattern" message:@"Your Email is in wrong formatte" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
//            alertEmail.tag=3001;
//            [alertEmail show];
//            
//        }
        
    }
    
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        if(textField==self.txtChildname ||textField==self.txtReasonforvisit )
        {
            if(self.flagChangingviewposition==1)
               // [self.viewScroll setContentOffset:CGPointMake(0, 0)];
                [self setViewMovedUp:NO];
        }
        
        if(textField==self.txtReasonforvisit)
        {
            [self.btnReasonforvisit setHidden:NO];
            
            
        }
//        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//         {
//             
//             if(textField==x && self.flagChangingviewposition==1)
//             {
//                 
//                // [self.viewScroll setContentOffset:CGPointMake(0, 0)];
//                  [self setViewMovedUp:NO];
//             }
//         }];
        
        [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
         {
             
             if(textField==x &&self.flagChangingviewposition==1)
             {
                // [self.viewScroll setContentOffset:CGPointMake(0, 0)];
                  [self setViewMovedUp:NO];
             }
         }];
        
    }
    
   else if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        if(textField==self.txtChildname ||textField==self.txtReasonforvisit ||textField==self.txtPhonenumber||textField==self.txtEmailaddress )
        {
            
            [UIView animateWithDuration:0.3 animations:^
             {
//                 CGRect rect = self.view.frame;
//                 self.view.frame =CGRectMake(0, 0 , rect.size.width, rect.size.height);
                   [self setViewMovedUp2:NO];
             }];
            
            
        }
        
        if (textField==self.txtParentfirstname || textField ==self.txtParentlastname) {
            
            [UIView animateWithDuration:0.3 animations:^
             {
//                 CGRect rect = self.view.frame;
//                 self.view.frame =CGRectMake(0, 0 , rect.size.width, rect.size.height);
                   [self setViewMovedUp2:NO];
                 
             }];
            
            
        }
        
        
//        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//         {
//             
//             if(textField==x && self.flagChangingviewposition==2)
//             {
////                 [UIView animateWithDuration:0.3 animations:^
////                  {
////                      CGRect rect = self.view.frame;
////                      self.view.frame =CGRectMake(0, 0 , rect.size.width, rect.size.height);
//                        [self setViewMovedUp2:NO];
//                      
////                  }];
//             }
//         }];
        
        
        [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
         {
             
             if(textField==x && self.flagChangingviewposition==2)
             {
//                 [UIView animateWithDuration:0.3 animations:^
//                  {
//                      CGRect rect = self.view.frame;
//                      self.view.frame =CGRectMake(0, 0 , rect.size.width, rect.size.height);
                        [self setViewMovedUp2:NO];
                      
//                  }];
             }
         }];
        
        if(textField==self.txtReasonforvisit)
        {
            [self.btnReasonforvisit setHidden:YES];
            
            
        }
    }
    
    
    
    
 else   if(UIDeviceOrientationFaceUp)
    {
        if(self.view.frame.size.height>900)
        {
          if(textField==self.txtChildname ||textField==self.txtReasonforvisit )
          {
              if(self.flagChangingviewposition==1)
                  //[self.viewScroll setContentOffset:CGPointMake(0, 0)];
                   [self setViewMovedUp:NO];
          }
          
          if(textField==self.txtReasonforvisit)
          {
              [self.btnReasonforvisit setHidden:NO];
              
              
          }
//          [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//           {
//               
//               if(textField==x && self.flagChangingviewposition==1)
//               {
//                   
//                  // [self.viewScroll setContentOffset:CGPointMake(0, 0)];
//                    [self setViewMovedUp:NO];
//               }
//           }];
          
          [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
           {
               
               if(textField==x &&self.flagChangingviewposition==1)
               {
                  // [self.viewScroll setContentOffset:CGPointMake(0, 0)];
                    [self setViewMovedUp:NO];
               }
           }];
    }
    else
    
      {
          if(textField==self.txtChildname ||textField==self.txtReasonforvisit ||textField==self.txtPhonenumber||textField==self.txtEmailaddress )
          {
              
//              [UIView animateWithDuration:0.3 animations:^
//               {
//                   CGRect rect = self.view.frame;
//                   self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +330 , rect.size.width, rect.size.height);

                     [self setViewMovedUp2:NO];
//               }];
              
              
          }
          
          if (textField==self.txtParentfirstname || textField ==self.txtParentlastname) {
              
//              [UIView animateWithDuration:0.3 animations:^
//              {
//                   CGRect rect = self.view.frame;
//                   self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +230 , rect.size.width, rect.size.height);
                 [self setViewMovedUp2:NO];
               
//               }];
              
              
          }
          
          
//          [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
//           {
//               
//               if(textField==x && self.flagChangingviewposition==2)
//               {
////                   [UIView animateWithDuration:0.3 animations:^
////                    {
////                        CGRect rect = self.view.frame;
////                        self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +330 , rect.size.width, rect.size.height);
//                        
//                          [self setViewMovedUp2:NO];
//                        
////                    }];
//               }
//           }];
          
          
          [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
           {
               
               if(textField==x && self.flagChangingviewposition==2)
               {
//                   [UIView animateWithDuration:0.3 animations:^
//                    {
//                        CGRect rect = self.view.frame;
//                        self.view.frame =CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +330 , rect.size.width, rect.size.height);
                        
                          [self setViewMovedUp2:NO];
//                    }];
               }
           }];
          
          if(textField==self.txtReasonforvisit)
          {
              [self.btnReasonforvisit setHidden:YES];
              
              
          }

      }
    }
    
  
    
    //return YES;
}

#pragma mark:- Taking phone Number in certain way
- (BOOL) textField:(UITextField *)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string {
    
   
         [self.viewCrossimage setHidden:NO];
    
    
    if(textField==self.txtPhonenumber)
    {
        // All digits entered
        if (range.location == 12) {
            return NO;
        }
        
        // Reject appending non-digit characters
        if (range.length == 0 &&
            ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
            return NO;
        }
        
        // Auto-add hyphen before appending 4rd or 7th digit
        if (range.length == 0 &&
            (range.location == 3 || range.location == 7)) {
            textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
            return NO;
        }
        
        // Delete hyphen when deleting its trailing digit
        if (range.length == 1 &&
            (range.location == 4 || range.location == 8))  {
            range.location--;
            range.length = 2;
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    return YES;
}



#pragma mark:-Type of Visitor
- (IBAction)actionBtncheckintype:(UIButton *)sender {
  
    // open a pop over listing Visitor type
    checkInouttypeTableViewController *popOverTableViewControllerobj=[[checkInouttypeTableViewController alloc]init];
    self.popOver =[[UIPopoverController alloc] initWithContentViewController:popOverTableViewControllerobj];
    self.popOver.popoverContentSize = CGSizeMake(170, 170);
    [self.popOver presentPopoverFromRect:self.btnCheckinouttype.frame inView:self.viewUppersection permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    popOverTableViewControllerobj.findVisitortype=^(NSString *visitorType)
    {
        self.txtSearchbar.placeholder=[NSString stringWithFormat:@"%@ List ",visitorType];
        if([visitorType isEqualToString:@"Customer"])
        {
           [self.btnReasonforvisit setHidden:YES];
            [self.btnReasonforvisit setUserInteractionEnabled:NO];
          
              UIColor *color = [UIColor whiteColor];
            self.txtReasonforvisit.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Where did you hear about us?" attributes:@{NSForegroundColorAttributeName: color}];
            [self backTodetail:visitorType];
            
             [self changePlaceholdercolor:self.txtReasonforvisit];
           

            
        }
        else {
            [self.btnReasonforvisit setHidden:NO];
             [self.btnReasonforvisit setUserInteractionEnabled:YES];
            [self.btnRemovechild setHidden:YES];
            [self.viewScroll setScrollEnabled:YES];
            UIColor *color = [UIColor whiteColor];
            self.txtReasonforvisit.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Reason for Visit?" attributes:@{NSForegroundColorAttributeName: color}];
            [self changeThelowerview:visitorType];
        }
        
        [self.popOver dismissPopoverAnimated:YES];
    };
   
}

#pragma mark:-Type of  lead source and reseason of visit
- (IBAction)actionBtnshowcontent:(id)sender {
    
    if(self.btnReasonforvisit.tag==1)
    {
//        leadSourceTableViewController *leadSourceTableViewControllerobj=[[leadSourceTableViewController alloc]init];
//        self.popOver =[[UIPopoverController alloc] initWithContentViewController:leadSourceTableViewControllerobj];
//        self.popOver.popoverContentSize = CGSizeMake(200, 200);
//        [self.popOver presentPopoverFromRect:self.btnReasonforvisit.frame inView:self.viewScroll permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
//        leadSourceTableViewControllerobj.findLeadsource=^(NSString *leadSource)
//        {
//            if (!([leadSource isEqualToString:@"others"])) {
//                self.txtReasonforvisit.text=leadSource;
//                [self.btnReasonforvisit setHidden:NO];
//                
//                
//            }
//            else
//            {
//                [self.btnReasonforvisit setHidden:YES];
//                [self.txtReasonforvisit becomeFirstResponder];
//                
//            }
//            [self.popOver  dismissPopoverAnimated:YES];
//        };
    }
    if(self.btnReasonforvisit.tag==2)
    {
        reasonForvisitTableViewController *reasonForvisitTableViewControllerobj =[[reasonForvisitTableViewController alloc]init];
        self.popOver=[[UIPopoverController alloc]initWithContentViewController:reasonForvisitTableViewControllerobj];
        self.popOver.popoverContentSize = CGSizeMake(200, 200);
        [self.popOver presentPopoverFromRect:self.btnReasonforvisit.frame inView:self.viewScroll permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        reasonForvisitTableViewControllerobj.reasonForvisitblock=^(NSString *reasonForvisit)
        {
            if (!([reasonForvisit isEqualToString:@"others"])) {
                self.txtReasonforvisit.text=reasonForvisit;
                [self.btnReasonforvisit setHidden:NO];
                
            }
            else
            {
                [self.btnReasonforvisit setHidden:YES];
                [self.txtReasonforvisit becomeFirstResponder];
                
            }
            [self.popOver  dismissPopoverAnimated:YES];
        };
        
    }
    
    
}








#pragma mark:-Add child
- (IBAction)actionBtnaddchild:(id)sender {
   
    [self.btnRemovechild setHidden:NO];
    
    if (self.addNumberofchild <=3)
    {
        self.addNumberofchild++;

                self.textFieldposition+=60;
        self.restrictScrollFrame=2;
       
        UITextField *addChildname =[[UITextField alloc]initWithFrame:CGRectMake(53, self.textFieldposition, 295, 30)];
        UIColor *colorChildname = [UIColor whiteColor];
        addChildname.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Child Name" attributes:@{NSForegroundColorAttributeName: colorChildname}];
        addChildname.borderStyle =UITextBorderStyleNone;
        [self drawLine:addChildname];
        [self.childNameObj addObject:addChildname];
        addChildname.tag=self.childNametag;
        addChildname.delegate=self;
        [self changeFont:addChildname];
        addChildname.textColor=[UIColor whiteColor];
        [self changeCursorcolor:addChildname];
        
        UITextField *addChildage;
        addChildage =[[UITextField alloc]init];
         self.contentHeightLandscape +=80;
         self.contentHeightPotrait +=80;
        if(UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
          
            addChildage.frame =CGRectMake(447, self.textFieldposition, 295, 30);
             self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width,  self.contentHeightPotrait);
        }
        if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
        {
          
            addChildage.frame =CGRectMake(647, self.textFieldposition, 295, 30);
             self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width,self.contentHeightLandscape);
        }
        else if([[UIDevice currentDevice]orientation]== UIDeviceOrientationFaceUp){
          
            if(self.view.frame.size.height  < 900){
               addChildage.frame =CGRectMake(647, self.textFieldposition, 295, 30);
           
                 self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width,  self.contentHeightPotrait);
                
            }else{
                
                addChildage.frame =CGRectMake(447, self.textFieldposition, 295, 30);
               
                 self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
            }
        }

       
        
        if(self.viewScroll.frame.size.height<self.viewScroll.contentSize.height)
        {
            [self.viewScroll setScrollEnabled:YES];
        }
        else
        {
              [self.viewScroll setScrollEnabled:NO];
        }
        
        UIColor *colorChildage = [UIColor whiteColor];
        addChildage.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Child Age" attributes:@{NSForegroundColorAttributeName: colorChildage}];
        addChildage.borderStyle =UITextBorderStyleNone;
        addChildage.textColor=[UIColor whiteColor];
        [self drawLine:addChildage];
        [self.childAgeObj addObject:addChildage];
        addChildage.delegate=self;
        
//        [addChildage addTarget:self action:@selector(childAgePopOver:) forControlEvents:UIControlEventEditingDidBegin];
        addChildage.tag=self.childAgetag;
        [self changeFont:addChildage];
        [self changeCursorcolor:addChildage];
        addChildage.keyboardType =UIKeyboardTypeNumberPad;
          
        [self.viewScroll addSubview:addChildage];
        [self.viewScroll addSubview:addChildname];
        
        if(self.viewScroll.frame.size.height <self.viewScroll.contentSize.height)
        {
            [self.viewScroll setScrollEnabled:YES];
        }
        else
        {
            [self.viewScroll setScrollEnabled:NO];
        }
        
        self.childAgetag++;
        self.childNametag++;
        
    }
    
    [self.viewScroll setScrollEnabled:YES];
    
}

#pragma mark:-Rest all the Field
- (IBAction)actionResetalltextfield:(id)sender {
    
    self.txtParentfirstname.text=@"";
    self.txtParentlastname.text=@"";
    self.txtPhonenumber.text=@"";
    self.txtReasonforvisit.text=@"";
    self.txtChildage.text=@"";
    self.txtChildname.text=@"";
    self.txtEmailaddress.text=@"";
    
    [self.childNameObj  enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
        x.text=@"";
     }];
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * y, NSUInteger index, BOOL *stop)
     {
         y.text=@"";
     }];
    
    
    
    
}

#pragma mark:- Remove child by clicking button

- (IBAction)actionRemovechild:(id)sender {
   
    if(self.childNametag>=101)
    {
    __block NSInteger flagFordeltename=0;
     __block NSInteger flagFordelteage=0;
   
    [self.childNameObj  enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         flagFordeltename=x.tag;
     }];
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         flagFordelteage=x.tag;
     }];
   
    UITextField *removeChildname =(UITextField *)[self.viewScroll viewWithTag:flagFordeltename];
    
    UITextField *removeChildage =(UITextField *)[self.viewScroll viewWithTag:flagFordelteage];
    [removeChildage removeFromSuperview];
    [removeChildname removeFromSuperview];
        
        
        
    self.addNumberofchild--;
    self.textFieldposition-=60;
        self.childNametag--;
        self.childAgetag--;
   
        if(self.childNametag==100)
        {
            [self.btnRemovechild setHidden:YES];
            
        }
        
         self.contentHeightLandscape-=80;
        self.contentHeightPotrait-=80;
        if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
        {
           
            self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
            
        }
        
        else if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
           
            self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightPotrait);
        }
        
        else if(UIDeviceOrientationFaceUp)
        {
            if(self.viewScroll.frame.size.height>900)
            {
               
                self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightPotrait);
            }
            else
            {
                
              
                self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
                
                
            }
        }
        
        
        
        if(self.viewScroll.contentSize.height>self.viewScroll.frame.size.height)
        {
            [self.viewScroll setScrollEnabled:YES];
        }
        else
        {
            [self.viewScroll setScrollEnabled:NO];
        }
    [self.childAgeObj removeLastObject];
    [self.childNameObj removeLastObject];
       
    }
    }


- (IBAction)childAgePopOver:(id)sender {
    
    
    
      //[self resignAllResponder];
    
    childAgePicker *popOverChildAgePicker=[[childAgePicker alloc]init];
    
       self.popOver =[[UIPopoverController alloc] initWithContentViewController:popOverChildAgePicker];
    self.popOver.popoverContentSize = CGSizeMake(170, 170);
    
    [self.popOver presentPopoverFromRect:((UITextField *)sender).frame inView:self.viewScroll permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    popOverChildAgePicker.childAgePicker=^(NSString *childAge)
    {
        if([childAge isEqualToString:@"-"])
        {
            ((UITextField *)sender).text=((UITextField *)sender).placeholder;
        }
        else
        ((UITextField *)sender).text=childAge;
    
        
     
        
    };

    
  
    
    
}

-(void)resignAllResponder
{
    for (UIView *view  in [self.viewScroll subviews]) {
        if ([view isKindOfClass:[UITextField class]])
        {
            [view resignFirstResponder];
        }
    }
    [self.txtParentfirstname resignFirstResponder];
     [self.txtParentlastname resignFirstResponder];
     [self.txtPhonenumber resignFirstResponder];
     [self.txtEmailaddress resignFirstResponder];
     [self.txtReasonforvisit resignFirstResponder];
     [self.txtChildname resignFirstResponder];
    [self.txtChildage resignFirstResponder];
    
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         [x resignFirstResponder
          ];
     }];
    
    [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * y, NSUInteger index, BOOL *stop)
     {
         [y resignFirstResponder];
     }];
    
    
    
    
}



#pragma mark:- change lower view on Visitor Type
-(void)changeThelowerview:(NSString *)visitor
{
   
    [self removeChildtextfield];
    self.lblHeaderinlowerview.text=[NSString stringWithFormat:@"%@ DETAIL ",[visitor uppercaseString]];
        UIColor *color = [UIColor whiteColor];
    self.txtChildname.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.txtChildname.frame=CGRectMake(53,223,295,30);
    [self.txtChildage setHidden:YES];
    [self.btnNewchild setHidden:YES];
    self.btnReasonforvisit.tag=2;
    
}
#pragma mark:-Remove child on changing visitor type

-(void) removeChildtextfield
{
    
    [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField * x, NSUInteger index, BOOL *stop)
     {
         [x removeFromSuperview];
     }];
    
    [self.childNameObj enumerateObjectsUsingBlock:^(UITextField * y, NSUInteger index, BOOL *stop)
     {
         [y removeFromSuperview];
     }];
    [self.childAgeObj removeAllObjects];
    [self.childNameObj removeAllObjects];
    [self adjustButtonPosition];
    
   
    
}
#pragma mark:-Adjust button position

-(void)adjustButtonPosition
{
    int positionOfbutton;
    
    for(positionOfbutton= 1;positionOfbutton<self.addNumberofchild;positionOfbutton++)
    {
          self.contentHeightPotrait-=80;
          self.contentHeightLandscape-=80;
       if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
       {
           
           self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
           
       }
        
        else if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
           
            self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightPotrait);
        }
        
        else if(UIDeviceOrientationFaceUp)
        {
            if(self.viewScroll.frame.size.height>900)
            {
                
                self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightPotrait);
            }
            else
            {
                
              
                self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
                

            }
        }
        
        
        
        
    }
    
    if(self.viewScroll.contentSize.height>self.viewScroll.frame.size.height)
    {
        [self.viewScroll setScrollEnabled:YES];
    }
    else
    {
        [self.viewScroll setScrollEnabled:NO];
    }

    self.addNumberofchild=1;
    self.textFieldposition=253;
    self.childNametag=100;
    self.childAgetag=200;
   
   
}

-(void)moveButtonup:(UIButton *)button
{
    [button removeFromSuperview];
    CGRect rect = button.frame;
    button.frame = CGRectMake(button.frame.origin.x,button.frame.origin.y-60, rect.size.width, rect.size.height);
    [self.viewScroll addSubview:button];
}

#pragma mark:- Notify Orientaion change of device
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [self.popOver  dismissPopoverAnimated:YES];
    [self.view endEditing:YES];
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
       
       
        self.viewImagebackground.frame=CGRectMake(0, 0, 1024, 630);
        self.viewForbackgroundcolor.frame=CGRectMake(0, 630, 1024, 138);
        self.viewUppersection.frame=CGRectMake(0,0, 1024, 320);
        self.btnCheckinouttype.frame=CGRectMake(156,220, 146, 58);
       
        
        self.btnCheckinouttype.frame=CGRectMake(156,170, 146, 58);
        self.viewSearchbox.frame=CGRectMake(400,170, 543, 58);
        self.viewsearchpng.frame=CGRectMake(411,185, 26, 30);
        self.txtSearchbar.frame=CGRectMake(445,178, 490, 44);
        self.lblHeaderinlowerview.frame=CGRectMake(325,255, 378, 28);
        self.viewCrossimage.frame=CGRectMake(910, 187, 25, 25);
         self.viewScroll.frame=CGRectMake(0 ,290, 1024, 288);
       
        
        self.btnReset.frame=CGRectMake(149 ,1, 189, 56);
        self.btnCheckin.frame=CGRectMake(385 ,1, 189, 56);
        self.btnCheckout.frame=CGRectMake(621 ,1, 189, 56);
        
        
        self.txtParentlastname.frame=CGRectMake(647 ,16, 295, 30);
        self.txtEmailaddress.frame=CGRectMake(647 ,89, 295, 30);
        self.txtReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
        self.txtChildage.frame=CGRectMake(647 ,253, 295, 30);
        
        self.btnReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
        self.btnRemovechild.frame=CGRectMake(899 ,198, 40, 40);

        [self.viewImagebackground setImage:[UIImage imageNamed:@"background.png"]];
        self.textFieldpositionfororientation=253;

        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField *x, NSUInteger index, BOOL *stop)
         {
             self.textFieldpositionfororientation+=60;
             x.frame =CGRectMake(647,self.textFieldpositionfororientation,295,30);
            
            
             
             

             self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);

             
         }];
      
        if(!(self.childAgeObj.count ==0))
        {
        self.btnCheckin.frame=CGRectMake(self.btnCheckin.frame.origin.x+30, self.btnCheckin.frame.origin.y, self.btnCheckin.frame.size.width, self.btnCheckin.frame.size.height);
        
        self.btnCheckout.frame=CGRectMake(self.btnCheckout.frame.origin.x+30, self.btnCheckout.frame.origin.y, self.btnCheckout.frame.size.width, self.btnCheckout.frame.size.height);
        
        self.btnReset.frame=CGRectMake(self.btnReset.frame.origin.x+30, self.btnReset.frame.origin.y, self.btnReset.frame.size.width, self.btnReset.frame.size.height);
        }
        
    }
   else if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
         [self.viewImagebackground setImage:[UIImage imageNamed:@"background.png"]];
        
        self.viewUppersection.frame=CGRectMake(0,0, 768, 349);
        self.viewImagebackground.frame=CGRectMake(0, 0, 768, 650);
        self.viewForbackgroundcolor.frame=CGRectMake(0,650, 768, 446);
        self.btnCheckinouttype.frame=CGRectMake(126,240, 146, 58);
       
        self.viewSearchbox.frame=CGRectMake(318,240, 343, 58);
        self.viewsearchpng.frame=CGRectMake(329,255, 26, 30);
        self.txtSearchbar.frame=CGRectMake(363,248, 290, 44);
        
          self.lblHeaderinlowerview.frame=CGRectMake(195,353, 378, 28);
         self.viewScroll.frame=CGRectMake(0 ,448, 768, 360);
         self.viewCrossimage.frame=CGRectMake(628, 257, 25, 25);
        
        self.btnReset.frame=CGRectMake(139 ,167, 139, 56);
        self.btnCheckin.frame=CGRectMake(315 ,167, 139, 56);
        self.btnCheckout.frame=CGRectMake(494 ,167, 139, 56);

        
        
        self.txtParentlastname.frame=CGRectMake(447 ,16, 295, 30);
        self.txtEmailaddress.frame=CGRectMake(447 ,89, 295, 30);
        self.txtReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
        self.txtChildage.frame=CGRectMake(447 ,253, 295, 30);
        
        self.btnReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
        self.btnRemovechild.frame=CGRectMake(649 ,198, 40, 40);
        
        self.textFieldpositionfororientation=253;

        [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField *x, NSUInteger index, BOOL *stop)
         {
            
             self.textFieldpositionfororientation+=60;
             x.frame =CGRectMake(447,self.textFieldpositionfororientation,295,30);

             self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width,  self.contentHeightPotrait);

             
         }];
        if(!(self.childAgeObj .count ==0))
        {
        self.btnCheckin.frame=CGRectMake(self.btnCheckin.frame.origin.x-30, self.btnCheckin.frame.origin.y, self.btnCheckin.frame.size.width, self.btnCheckin.frame.size.height);
        
        self.btnCheckout.frame=CGRectMake(self.btnCheckout.frame.origin.x-30, self.btnCheckout.frame.origin.y, self.btnCheckout.frame.size.width, self.btnCheckout.frame.size.height);
        
        self.btnReset.frame=CGRectMake(self.btnReset.frame.origin.x-30, self.btnReset.frame.origin.y, self.btnReset.frame.size.width, self.btnReset.frame.size.height);
        }
    }
    
   else if(UIDeviceOrientationFaceUp)
    {
        if(self.view.frame.size.height>900)
        {
            [self.viewImagebackground setImage:[UIImage imageNamed:@"background.png"]];
            
            self.viewUppersection.frame=CGRectMake(0,0, 768, 349);
            self.viewImagebackground.frame=CGRectMake(0, 0, 768, 650);
            self.viewForbackgroundcolor.frame=CGRectMake(0,650, 768, 446);
            self.btnCheckinouttype.frame=CGRectMake(126,240, 146, 58);
            
            self.viewSearchbox.frame=CGRectMake(318,240, 343, 58);
            self.viewsearchpng.frame=CGRectMake(329,255, 26, 30);
            self.txtSearchbar.frame=CGRectMake(363,248, 290, 44);
            
            self.lblHeaderinlowerview.frame=CGRectMake(195,353, 378, 28);
            self.viewScroll.frame=CGRectMake(0 ,448, 768, 360);
             self.viewCrossimage.frame=CGRectMake(628, 257, 25, 25);
            
            self.btnReset.frame=CGRectMake(139 ,167, 139, 56);
            self.btnCheckin.frame=CGRectMake(315 ,167, 139, 56);
            self.btnCheckout.frame=CGRectMake(494 ,167, 139, 56);

            
            
            self.txtParentlastname.frame=CGRectMake(447 ,16, 295, 30);
            self.txtEmailaddress.frame=CGRectMake(447 ,89, 295, 30);
            self.txtReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
            self.txtChildage.frame=CGRectMake(447 ,253, 295, 30);
            
            self.btnReasonforvisit.frame=CGRectMake(53 ,157, 679, 30);
            self.btnRemovechild.frame=CGRectMake(649 ,198, 40, 40);
            
            self.textFieldpositionfororientation=253;

            [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField *x, NSUInteger index, BOOL *stop)
             {
                
                 self.textFieldpositionfororientation+=60;
                 x.frame =CGRectMake(447,self.textFieldpositionfororientation,295,30);
                 

                   self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);

                     self.contentHeightPotrait+=80;
                     self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightPotrait);
                     self.restrictScrollFrame=1;
                     

                 
             }];
            if(!(self.childAgeObj .count ==0))
            {
                self.btnCheckin.frame=CGRectMake(self.btnCheckin.frame.origin.x-30, self.btnCheckin.frame.origin.y, self.btnCheckin.frame.size.width, self.btnCheckin.frame.size.height);
                
                self.btnCheckout.frame=CGRectMake(self.btnCheckout.frame.origin.x-30, self.btnCheckout.frame.origin.y, self.btnCheckout.frame.size.width, self.btnCheckout.frame.size.height);
                
                self.btnReset.frame=CGRectMake(self.btnReset.frame.origin.x-30, self.btnReset.frame.origin.y, self.btnReset.frame.size.width, self.btnReset.frame.size.height);
            }
        }
        else{
            self.viewImagebackground.frame=CGRectMake(0, 0, 1024, 630);
            self.viewForbackgroundcolor.frame=CGRectMake(0, 630, 1024, 138);
            self.viewUppersection.frame=CGRectMake(0,0, 1024, 320);
            self.btnCheckinouttype.frame=CGRectMake(156,220, 146, 58);
            
            
            self.btnCheckinouttype.frame=CGRectMake(156,170, 146, 58);
            self.viewSearchbox.frame=CGRectMake(400,170, 543, 58);
            self.viewsearchpng.frame=CGRectMake(411,185, 26, 30);
            self.txtSearchbar.frame=CGRectMake(445,178, 490, 44);
            self.lblHeaderinlowerview.frame=CGRectMake(325,255, 378, 28);
            
            self.viewScroll.frame=CGRectMake(0 ,290, 1024, 288);
            self.btnReset.frame=CGRectMake(149 ,1, 189, 56);
            self.btnCheckin.frame=CGRectMake(385 ,1, 189, 56);
            self.btnCheckout.frame=CGRectMake(621 ,1, 189, 56);
             self.viewCrossimage.frame=CGRectMake(910, 187, 25, 25);

            
            self.txtParentlastname.frame=CGRectMake(647 ,16, 295, 30);
            self.txtEmailaddress.frame=CGRectMake(647 ,89, 295, 30);
            self.txtReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
            self.txtChildage.frame=CGRectMake(647 ,253, 295, 30);
            
            self.btnReasonforvisit.frame=CGRectMake(53 ,157, 750, 30);
            self.btnRemovechild.frame=CGRectMake(899 ,198, 40, 40);

            [self.viewImagebackground setImage:[UIImage imageNamed:@"background.png"]];
            self.textFieldpositionfororientation=253;
             self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
            
            [self.childAgeObj enumerateObjectsUsingBlock:^(UITextField *x, NSUInteger index, BOOL *stop)
             {
                 
                 self.textFieldpositionfororientation+=60;
                 x.frame =CGRectMake(647,self.textFieldpositionfororientation,295,30);
                 
              
                self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
              
                     self.contentHeightLandscape+=80;
                     self.viewScroll.contentSize =CGSizeMake(self.viewScroll.frame.size.width, self.contentHeightLandscape);
                     self.restrictScrollFrame=1;

                 
             }];
            
            if(!(self.childAgeObj.count ==0))
            {
                self.btnCheckin.frame=CGRectMake(self.btnCheckin.frame.origin.x+30, self.btnCheckin.frame.origin.y, self.btnCheckin.frame.size.width, self.btnCheckin.frame.size.height);
                
                self.btnCheckout.frame=CGRectMake(self.btnCheckout.frame.origin.x+30, self.btnCheckout.frame.origin.y, self.btnCheckout.frame.size.width, self.btnCheckout.frame.size.height);
                
                self.btnReset.frame=CGRectMake(self.btnReset.frame.origin.x+30, self.btnReset.frame.origin.y, self.btnReset.frame.size.width, self.btnReset.frame.size.height);
            }
        }
        
    }
   
    

}

//- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//  [scrollView  setContentOffset:CGPointMake(self.viewScroll.contentSize.width,self.viewScroll.frame.origin.y) animated:YES];
//}

#pragma mark:- reset button position
-(void)backTodetail:(NSString *)visitor
{
    
   
    self.lblHeaderinlowerview.text=[NSString stringWithFormat:@"%@ DETAIL ",[visitor uppercaseString]];
    UIColor *color = [UIColor whiteColor];
    self.txtChildname.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Child Name" attributes:@{NSForegroundColorAttributeName: color}];
     self.txtChildname.frame=CGRectMake(53, 253, 295, 30);
    [self.txtChildage setHidden:NO];
    [self.btnNewchild setHidden:NO];
    self.btnReasonforvisit.tag=1;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    [aScrollView setContentOffset: CGPointMake(0, aScrollView.contentOffset.y)];
    
}


-(void)changeTheposition:(UIButton *)button
{
   [button removeFromSuperview];
  
   button.frame=CGRectMake(button.frame.origin.x, button.frame.origin.y+60,button.frame.size.width, button.frame.size.height);
      [_viewForbackgroundcolor addSubview:button];
 
}
- (IBAction)btnCheckin:(id)sender {
    [self actionResetalltextfield:nil];
}

- (IBAction)btnCheckout:(id)sender {
    CheckInOutViewController *objCheckInOutViewController=[[CheckInOutViewController alloc]initWithNibName:@"CheckInOutViewController" bundle:nil];
    [self.navigationController pushViewController:objCheckInOutViewController animated:YES];
}



#pragma mark:- alert view delegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag==3001)
//    {
//        [self.txtEmailaddress becomeFirstResponder];
//    }
//}
@end

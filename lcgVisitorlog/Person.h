
//  Person.h
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property(nonatomic,strong) NSString *imageCheckInOut;
@property(nonatomic,strong) NSString *imageMgmtValidation;
@property(nonatomic,strong) NSString *personType;
@property(nonatomic,strong) NSString *isChecked;
@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *isMgmtValidated;
@property(nonatomic,strong) NSDate *checkInTime;
@property(nonatomic,strong) NSDate *checkOutTime;
@property(nonatomic,strong) NSString *leadSource;
@property(nonatomic,strong) NSString *leadTotalChilds;
@property(nonatomic,strong) NSString *reasonToVisit;

@end

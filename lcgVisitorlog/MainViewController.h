//
//  MainViewController.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/16/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>
-(void)changeThelowerview:(NSString *)visitor;
-(void)backTodetail:(NSString *)visitor;
-(void)drawLine:(UITextField*)textField;
-(void)changePlaceholdercolor:(UITextField*)textField;
-(void)makeButtonround:(UIButton *)button;
-(void)checkAndManagementScreen;
-(void)settingScreen;
-(void)changeTheposition:(UIButton *)button;
-(void)changeCursorcolor:(UITextField *)textField;
-(void)changeFont:(UITextField *)textField;
-(void)changeFontLowerSectionHeader:(UILabel *)label;
-(void)changeButtonFont:(UIButton *)button;
-(void)changeImage:(UIImage *)image xposition:(NSInteger *)x ypostion:(NSInteger *)y width:(NSInteger *)width height:(NSInteger *)height;

@end

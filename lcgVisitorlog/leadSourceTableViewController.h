//
//  leadSourceTableViewController.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface leadSourceTableViewController : UITableViewController
@property(nonatomic,copy)void (^findLeadsource)(NSString *typeOfleadsource);
@end

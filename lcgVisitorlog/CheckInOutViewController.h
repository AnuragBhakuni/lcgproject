//
//  CheckInOutViewController.h
//  lcgVisitorlog
//
//  Created by Vaibhav Kumar on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataCheckInOut.h"

#import "CheckInOutTableViewCell.h"


#import "NonLeadEditorView.h"
#import "LeadEditorUiView.h"




@interface CheckInOutViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    NSDateFormatter *dateFormatter;
}



@property (weak, nonatomic) IBOutlet UIImageView *imageAtuiViewBg;

@property (strong, nonatomic) IBOutlet LeadEditorUiView *uiViewLeadMain;
@property (weak, nonatomic) IBOutlet UIView *uiViewChild;

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *uiViewInner;


@property (weak, nonatomic) IBOutlet UIImageView *cancelImageBtn;

@property (nonatomic,strong) NSMutableArray *objPersonCell;

@property (nonatomic,strong)IBOutlet LeadEditorUiView *objLeadEditorUIView;



@end

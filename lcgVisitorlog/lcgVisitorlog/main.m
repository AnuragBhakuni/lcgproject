//
//  main.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/14/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

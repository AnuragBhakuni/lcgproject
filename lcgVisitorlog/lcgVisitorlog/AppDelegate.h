//
//  AppDelegate.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/14/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *objnavigationController;
//@property (strong, nonatomic) UINavigationController *objNavcontroller;

@end

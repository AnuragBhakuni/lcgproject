//
//  SettingScreenViewController.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/18/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingScreenViewController : UIViewController
-(void)drawLine:(UITextField*)textField;
@end

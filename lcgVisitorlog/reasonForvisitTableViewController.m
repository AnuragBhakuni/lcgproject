//
//  reasonForvisitTableViewController.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/16/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "reasonForvisitTableViewController.h"

@interface reasonForvisitTableViewController ()
{
    NSArray *reasonForvisit;
}

@end

@implementation reasonForvisitTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableFooterView =[[UIView alloc] initWithFrame:CGRectZero];
    reasonForvisit =[[NSArray alloc]initWithObjects:@"Enquiry",@"Meeting",@"Repairing",@"others",nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return reasonForvisit.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowNUmber;
    static NSString *simpleTableIdentifier = @"SimpleTable";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:simpleTableIdentifier];
    }
    rowNUmber=indexPath.row;
    cell.textLabel.text = [reasonForvisit objectAtIndex:rowNUmber];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell =(UITableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    self.reasonForvisitblock(cell.textLabel.text);
    
    
}


@end

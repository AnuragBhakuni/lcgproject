//
//  checkInouttypeTableViewController.m
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/15/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import "checkInouttypeTableViewController.h"

@interface checkInouttypeTableViewController ()

{
    NSArray *checkInouttype;
}
@end

@implementation checkInouttypeTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView =[[UIView alloc] initWithFrame:CGRectZero];
    checkInouttype =[[NSArray alloc]initWithObjects:@"Customer",@"Vendor/Partner",@"Volunteer",@"Other",nil];
  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return checkInouttype.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowNUmber;
    static NSString *simpleTableIdentifier = @"SimpleTable";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:simpleTableIdentifier];
    }
    rowNUmber=indexPath.row;
    if(rowNUmber==0)
    {
   
        cell.contentView.backgroundColor=[UIColor colorWithRed:249/255.0f green:245/255.0f blue:211/255.0f alpha:1.0f];
    }
    else if(rowNUmber==1)
    {
         cell.contentView.backgroundColor=[UIColor colorWithRed:231/255.0f green:241/255.0f blue:195/255.0f alpha:1.0f];
    }
    else if (rowNUmber==2)
    {
         cell.contentView.backgroundColor= [UIColor colorWithRed:189/255.0f green:226/255.0f blue:250/255.0f alpha:1.0f];;
        
    }
    else
    {
         cell.contentView.backgroundColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f];
    }
    cell.textLabel.text = [checkInouttype objectAtIndex:rowNUmber];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
 
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell =(UITableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    self.findVisitortype(cell.textLabel.text);   
    
}


@end

//
//  reasonForvisitTableViewController.h
//  lcgVisitorlog
//
//  Created by Anurag Bhakuni on 7/16/14.
//  Copyright (c) 2014 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface reasonForvisitTableViewController : UITableViewController
@property(nonatomic,copy)void (^reasonForvisitblock)(NSString *reasonForvisit);

@end
